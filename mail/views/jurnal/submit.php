<?php
use yii\helpers\Html;
?>
    <table class="body-wrap">
	<tr>
            <td></td>
            <td class="container" bgcolor="#FFFFFF">
                <div class="content">
                    <table>
                        <tr>
                            <td>                                
                                <!--p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                                <p><img src="http://pesstaplus.raharja.me/logo.png" /></p>< /pdeddy78 -->
                                <h3>Dear, <?= Yii::$app->user->identity->Username;?></h3>
                                <!--p class="callout">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod dolor sit amet, consectetur adipisicing elit. <a href="#">Do it Now! &raquo;</a>
                                </p-->
                            </td>
                        </tr>
                    </table>
                </div>						
            </td>
            <td></td>
	</tr>
    </table>
    <div class="column-wrap">			
        <div class="column">
            <table align="left">
                <tr>
                    <td>
                        <p>Terima kasih Anda telah melakukan submit di PESSTA+. Berikut ini detail dari hasil submit yang Anda lakukan:</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                        <a class="btn">Lihat Submit &raquo;</a>
                        <p>Proses selanjutnya akan dilakukan validasi keabsahan oleh admin. Mohon silakan tunggu maksimal 4x24 jam untuk diinformasikan hasilnya.</p>
                        <p>Regards,<br><br>PESSTA+ Team</p>
                    </td>
                </tr>
            </table>
        </div>			
	<div class="column">
            <table align="left">
                <tr>
                    <td>
                        <ul class="sidebar">
                            <li>
                                <a>
                                    <h5>Jurnal &raquo;</h5>
                                    <p>Detail Submit ...</p>
                                </a>
                            </li>
                            <li><img src="https://lh3.googleusercontent.com/UebCymoYAjFShSUH9FvYEaVYkIMpCKHGVK51FUvcmw9q6tldWFVtI4lCiqfjIWTfqM2eoPj6LA=w1366-h768-rw-no" /></li>
                            <li><a class="last">Just a Plain Link &raquo;</a></li>
                        </ul>
                        <!-- social & contact -->
                        <table bgcolor="" class="social" width="100%">
                            <tr>
                                <td>
                                    <table align="left" width="100%">
                                        <tr>
                                            <td>
                                                <h6 class="">Connect with Us:</h6>
                                                <p class=""><a href="http://goo.gl/QxNFDi" class="soc-btn gp">Google+</a></p>
                                                <h6 class="">Contact Info:</h6>												
                                                <p>Email: <strong><?= Html::a('pesstaplus@raharja.info', null, ['href' => 'mailto:pesstaplus@raharja.info']);?></strong>
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>					
        </div>				
        <div class="clear"></div>
    </div>