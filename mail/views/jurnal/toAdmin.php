<?php
use yii\helpers\Html;
?>
    <table class="body-wrap">
	<tr>
            <td></td>
            <td class="container" bgcolor="#FFFFFF">
                <div class="content">
                    <table>
                        <tr>
                            <td>
                                <h3>Dear, Admin</h3>
                                <p class="lead">Jurnal baru saja disubmit,</p>
                                <p>Atas nama <?=Yii::$app->user->identity->Username;?> telah melakukan submit di PESSTA+. Dan berikut ini detail dari hasil submit:</p>
                                <p class="callout">
                                    Jurnal milik Deddy Pratama yang berjudul 
                                    <strong>APLIKASI SISTEM INFORMASI SIMPAN PINJAM PADA KOPERASI USAHA BERSAMA SYARI’AH AT-TAHWIL KOTA TANGERANG</strong> dari TEKNOMEDIA 
                                    sudah berhasil disubmit dari Alamat Rinfo <?=Yii::$app->user->identity->AlamatEmail?> dan 
                                    telah disimpan dalam PESSTA+ pada waktu <?=time()?>.
                                </p>
                                <p>Proses selanjutnya untuk segera dilakukan validasi keabsahan. Terima kasih.</p>
                                <p>Regards, <br><br>PESSTA+ Team</p>
                                <table class="social" width="100%">
                                    <tr>
                                        <td>
                                            <table align="left" class="column">
                                                <tr>
                                                    <td>				

                                                        <h5 class="">Connect with Us:</h5>
                                                        <p class=""><a href="http://goo.gl/QxNFDi" class="soc-btn gp">Google+</a></p>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table align="left" class="column">
                                                <tr>
                                                    <td>
                                                        <h5 class="">Contact Info:</h5>												
                                                        <p>Email: <strong><?= Html::a('pesstaplus@raharja.info', null, ['href' => 'mailto:pesstaplus@raharja.info']);?></strong></p>                
                                                    </td>
                                                </tr>
                                            </table>									
                                            <span class="clear"></span>									
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>			
            </td>
            <td></td>
	</tr>
    </table>
