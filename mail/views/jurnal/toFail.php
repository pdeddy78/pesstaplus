<?php
use yii\helpers\Html;
?>
    <table class="body-wrap">
	<tr>
            <td></td>
            <td class="container" bgcolor="#FFFFFF">
                <div class="content">
                    <table>
                        <tr>
                            <td>
                                <h3>Dear, <?= Yii::$app->user->identity->Username;?></h3>
                                <p class="lead">Hasil Cek Validasi,</p>
                                <p>Maaf, berdasarkan hasil cek validasi, PESSTA+ tidak bisa menerima keabsahan dari submit yang telah kamu lakukan pada tanggal dikarenakan:</p>
                                <p class="callout">
                                    cuman tes speed submit
                                </p>
                                <p>Regards,<br><br>PESSTA+ Team</p>
                                <table class="social" width="100%">
                                    <tr>
                                        <td>
                                            <table align="left" class="column">
                                                <tr>
                                                    <td>				

                                                        <h5 class="">Connect with Us:</h5>
                                                        <p class=""><a href="http://goo.gl/QxNFDi" class="soc-btn gp">Google+</a></p>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table align="left" class="column">
                                                <tr>
                                                    <td>
                                                        <h5 class="">Contact Info:</h5>												
                                                        <p>Email: <strong><?= Html::a('pesstaplus@raharja.info', null, ['href' => 'mailto:pesstaplus@raharja.info']);?></strong></p>                
                                                    </td>
                                                </tr>
                                            </table>									
                                            <span class="clear"></span>									
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>			
            </td>
            <td></td>
	</tr>
    </table>
