<?php
use yii\helpers\Html;
?>
    <table class="body-wrap">
	<tr>
            <td></td>
            <td class="container" bgcolor="#FFFFFF">
                <div class="content">
                    <table>
                        <tr>
                            <td>
                                <h3>Dear, <?= Yii::$app->user->identity->Username;?></h3>
                                <p class="lead">Congratulatioooon,</p>
                                <p>Woohoooo.... Selamat yaa ...!</p>
                                <p class="callout">
                                    Jurnal <strong>APLIKASI PROTOTYPE SISTEM INFORMASI PENERIMAAN SISWA BARU PADA SMK YUPPENTEK 1 TANGERANG BERBASIS WEB</strong> 
                                    yang kamu submit <?=time()?> sudah dinyatakan valid.
                                </p>
                                <div class="column-wrap">
                                    <table align="left">
                                        <tbody>
                                            <tr>
                                                <td colspan="3">
                                                <ul class="sidebar">
                                                    <li>
                                                        <a href="#">
                                                        <h5>Jurnal »</h5>
                                                        <p>Detail Submit ...</p>
                                                        </a>
                                                    </li>                                                                
                                                </ul>                                   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td rowspan="3">
                                                    <?= Html::img("https://lh3.googleusercontent.com/UebCymoYAjFShSUH9FvYEaVYkIMpCKHGVK51FUvcmw9q6tldWFVtI4lCiqfjIWTfqM2eoPj6LA=w1366-h768-rw-no")?>
                                                </td>
                                                <td>Paper</td>
                                                <td>ANALISA</td>
                                            </tr>
                                            <tr>
                                                <td>Nama Jurnal</td>
                                                <td>CCIT</td>
                                            </tr>
                                            <tr>
                                                <td>Valid</td>
                                                <td>123131312313</td>
                                            </tr>
                                        </tbody>
                                    </table>				
                                    <div class="clear"></div>
                                </div>
                                <p>Silakan cek kembali status validasi kamu di PESSTA+.</p>
                                <a href="http://pesstaplus.raharja.me" class="btn">Cari Validasi &raquo;</a>
                                <p>Regards,<br><br>PESSTA+ Team</p>
                                <table class="social" width="100%">
                                    <tr>
                                        <td>
                                            <table align="left" class="column">
                                                <tr>
                                                    <td>				

                                                        <h5 class="">Connect with Us:</h5>
                                                        <p class=""><a href="http://goo.gl/QxNFDi" class="soc-btn gp">Google+</a></p>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table align="left" class="column">
                                                <tr>
                                                    <td>
                                                        <h5 class="">Contact Info:</h5>												
                                                        <p>Email: <strong><?= Html::a('pesstaplus@raharja.info', null, ['href' => 'mailto:pesstaplus@raharja.info']);?></strong></p>                
                                                    </td>
                                                </tr>
                                            </table>									
                                            <span class="clear"></span>									
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>			
            </td>
            <td></td>
	</tr>
    </table>
