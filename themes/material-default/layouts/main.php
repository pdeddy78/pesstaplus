<?php
use yii\helpers\Html;
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;
use yii\widgets\Spaceless;
use app\assets\AppAsset;
use lo\modules\noty\Wrapper;

\ercling\pace\PaceWidget::widget([
    'color'=>'green',
    'theme'=>'minimal',
    'options'=>[
        'ajax'=>['trackMethods'=>['GET','POST']]
    ]
]);

echo Wrapper::widget([
    'layerClass' => 'lo\modules\noty\layers\Growl',
]);
/**
 * @var $this \yii\base\View
 * @var $content string
 */
// $this->registerAssetBundle('app');
?>

<?php $this->beginPage(); ?>

<!DOCTYPE html>
<html lang="id">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="author" content="pdeddy78" />
  <meta name="description" content="Penilaian Sidang Skripsi dan Tugas Akhir Plus, PESSTA+ Raharja, Pesstaplus" />
  <meta name="theme-color" content="#66BB6A" />
  <meta name="msapplication-navbutton-color" content="#66BB6A" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="apple-mobile-web-app-status-bar-style" content="#66BB6A" />
  <title><?=Html::encode($this->title); ?> <?=Yii::$app->name?> </title>
  <?php $this->head();?>
  <?= Html::csrfMetaTags() ?>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>  
  <!-- CSS  -->
  <link href="<?php echo $this->theme->baseUrl ?>/css/materialize_normalize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="<?php echo $this->theme->baseUrl ?>/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
  <?php $this->beginBody() ?>
  <?php Spaceless::begin(); ?>
  <nav class="green lighten-1" role="navigation">
    <div class="container">
        <div class="nav-wrapper"><a id="logo-container" href="#" class="brand-logo"><?=Html::encode(\Yii::$app->name); ?></a>
	<?php
            
            $menuItems = [
                ['label' => 'Beranda', 'url' => ['/site/index'],'options'=>['class'=>'waves-effect waves-light']],
		['label' => 'Tentang', 'url' => ['/site/about'],'options'=>['class'=>'waves-effect waves-light']],
		//['label' => 'Kontak', 'url' => ['/site/contact'],'options'=>['class'=>'waves-effect']],
		//['label' => 'Log', 'url' => ['/actionlog/log/index']]
            ];
            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => 'Masuk', 'url' => ['/site/auth?authclient=google'],'options'=>['class'=>'waves-effect waves-light']];
            } else {
                $menuItems[] = ['label' => 'Keluar (' . Yii::$app->user->identity->Username . ')', 
                                'url' => ['/site/logout'],
                                'template' => '<a href="{url}" data-method="post">{label}</a>',
                                'options'=>['class'=>'waves-effect waves-light']
                                ];
            }
            
            echo Menu::widget([
                'options' => ['id' => "nav-mobile", 'class' => 'right side-nav'],
                'items' => $menuItems
            ]);	
	?>
        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
      </div>
    </div>
  </nav>

  <div class="container">
    <div class="section">
      <div class="row">
        <div class="col s12 m12">
          <?php echo $content; ?>
        </div>
      </div>
    </div>
  </div>
    <div class="fixed-action-btn vertical click-to-toggle" style="bottom: 45px; right: 24px;">
        <a class="btn-floating btn-large green darken-3">
            <i class="mdi-action-view-list"></i>
        </a>
        <ul>
            <li><a class="btn-floating modal-trigger red tooltipped" data-position="bottom" data-delay="50" data-tooltip="Submit PESSTA+" href="#modal1"><i class="mdi-file-file-upload"></i></a></li>
            <li><a class="btn-floating modal-trigger yellow darken-1 tooltipped" data-position="bottom" data-delay="50" data-tooltip="My PESSTA+" href="#modal4"><i class="mdi-file-attachment"></i></a></li>
            <li><a class="btn-floating modal-trigger green tooltipped" data-position="bottom" data-delay="50" data-tooltip="Cek Validasi" href="#modal2"><i class="mdi-social-school"></i></a></li>
            <li><a class="btn-floating modal-trigger blue tooltipped" data-position="bottom" data-delay="50" data-tooltip="Kelola PESSTA+" href="#modal3"><i class="mdi-social-notifications"></i></a></li>
            <li><a class="btn-floating pink tooltipped" data-position="bottom" data-delay="50" data-tooltip="Viewboard PESSTA+" href="#"><i class="mdi-action-trending-up"></i></a></li>
        </ul>
    </div>
  <footer class="page-footer green">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">About PESSTA+</h5>
          <p class="grey-text text-lighten-4" align="justify"><strong>PESSTA+</strong> adalah sebuah sistem validasi jurnal dan hibah yang dilakukan oleh mahasiswa secara online dan mandiri. Guna membuktikan bahwa mahasiswa atau Pribadi Raharja telah membuat sebuah artikel ilmiah yang lolos atau telah terbit di jurnal yang diakui dalam <u><strong><a class="white-text" href="skptr.ilearning.me/2016/08/21/821sk-ketentuan-jurnalptviii2016-tentang-ketentuan-jurnal-ilmiah-yang-diakui-untuk-penilaian-objektif-di-perguruan-tinggi-raharja" target="_blank">SK No 821 Tentang Ketentuan Jurnal Ilmiah Yang Diakui Untuk Penilaian Objektif Di Perguruan Tinggi Raharja</a></strong></u> maka perlu dilakukan validasi jurnal di PESSTA+, begitu juga untuk membuktikan nilai hibah mahasiswa.</p>
        </div>
        <div class="col l3 s12">
          <h5 class="white-text">Pranala Cepat</h5>
          <ul>
            <li><a class="white-text" href="http://hibah.ilearning.me/" target="_blank">iMe Hibah</a></li>
            <li><a class="white-text" href="<?=Yii::$app->getUrlManager()->createUrl(['api'])?>" target="_blank">API</a></li>
            <!--li><a class="white-text" href="#!">TOS</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li-->
          </ul>
        </div>
        <div class="col l3 s12">
          <h5 class="white-text">How can we help?</h5>
          <ul>
            <li class="center"><a id="mibew-agent-button" href="http://iduhelp.magics.co/chat?locale=en" target="_blank"><img src="http://iduhelp.magics.co/b?i=mibew&amp;lang=en" border="0" alt=""></li>
            <li class="center"><a href="http://iran.ilearning.me/2015/02/04/etalase-iran/" target="_blank"><img class="aligncenter size-full wp-image-55" alt="iran" src="https://lh6.googleusercontent.com/-c1xLfkA4Im8/Vw8Pn3EsJiI/AAAAAAAAEW0/JABK3XovRosfirKVlEQrwzHy8x0EhDG5ACL0B/w432-h216-no/Logo%2BiRAN%2B3.png" width="210px" height="80px"></a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Made with <i class="mdi-action-favorite red-text"></i> by <a class="green-text text-lighten-4" href="http://widuri.raharja.info/index.php?title=PESSTA%2B" target="_blank">PESSTA+</a>. 
      </div>
    </div>
  </footer>  


  <!--  Scripts -->
  <?php // $this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);?>
  <?php $this->registerJsFile($this->theme->baseUrl.'/js/materialize.js', ['depends' => [\yii\web\JqueryAsset::className()]]);?>
  <?php $this->registerJsFile($this->theme->baseUrl.'/js/init.js', ['depends' => [\yii\web\JqueryAsset::className()]]);?>
  <?php Spaceless::end(); ?>
  <?php $this->endBody() ?>
  </body>
</html>
<?php $this->endPage(); ?>