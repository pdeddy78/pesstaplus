(function($){
  $(function(){
    $('.button-collapse').sideNav({
        menuWidth: 300,
        edge: 'left',
        closeOnClick: true,
    });
    $('.tooltipped').tooltip({delay: 50});
    $('input#input_text, textarea#textarea1').characterCounter();
    //$('.fixed-action-btn').openFAB();
    //$('.fixed-action-btn').closeFAB();
  }); // end of document ready
})(jQuery); // end of jQuery name space