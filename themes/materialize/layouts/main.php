<?php
use yii\helpers\Html;
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;
use yii\widgets\Spaceless;
use app\assets\MaterialAsset;
use lo\modules\noty\Wrapper;
use yii\web\View;

\ercling\pace\PaceWidget::widget([
    'color'=>'green',
    'theme'=>'minimal',
    'options'=>[
        'ajax'=>['trackMethods'=>['GET','POST']]
    ]
]);
echo Wrapper::widget([
    'layerClass' => 'lo\modules\noty\layers\Growl',
    'options' => [
       'fixed' => true,
       'size' => 'medium',
       'location' => 'tr',
       'delayOnHover' => true,
    ],
]);
/**
 * @var $this \yii\base\View
 * @var $content string
 */
// $this->registerAssetBundle('app');
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="author" content="pdeddy78" />
  <meta name="description" content="Penilaian Sidang Skripsi dan Tugas Akhir Plus, PESSTA+ Raharja, Pesstaplus" />
  <meta name="theme-color" content="#66BB6A" />
  <meta name="msapplication-navbutton-color" content="#66BB6A" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="apple-mobile-web-app-status-bar-style" content="#66BB6A" />
  <title><?=Html::encode($this->title); ?> | <?=Yii::$app->name?> </title>
  <?php MaterialAsset::register($this); ?>
  <?php $this->head();?>
  <?= Html::csrfMetaTags() ?>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>  
</head>
<body>
  <?php $this->beginBody() ?>
  <?php Spaceless::begin(); ?>
    <div class="navbar-fixed">
        <nav class="green lighten-1" role="navigation">
            <div class="nav-wrapper container"><a id="logo-container" href="<?=Yii::$app->getUrlManager()->createUrl(['site/home'])?>" class="brand-logo waves-effect default"><?=Html::encode(\Yii::$app->name); ?></a>
              <?php
                  if (!Yii::$app->user->isGuest) {
                      $menuItems = [
                            ['label' => 'Submit', 'url' => ['/jurnal/jurnalUpload/submit']],
                            ['label' => 'Koleksi', 'url' => ['/site/contact']],
                            ['label' => 'Validasi', 'url' => ['/validasi']],
                            ['label' => 'Kelola Jurnal', 'url' => ['/jurnal/jurnalUpload/admin']],
                            ['label' => 'Kelola Hibah', 'url' => ['/hibah/hibahUpload/admin']],
                        ];
                  }
                  if (Yii::$app->user->isGuest) {
                    //$menuItems[] = ['label' => 'Beranda', 'url' => ['/site/index']];
                      $menuItems[] = ['label' => 'Masuk', 'url' => ['/site/auth?authclient=google']];
                  } else {
                      $menuItems[] = ['label' => 'Keluar (' . Yii::$app->user->identity->Username . ')', 
                                      'url' => ['/site/logout'],
                                      'template' => '<a href="{url}" data-method="post">{label}</a>'
                                      ];
                  }

                  echo Menu::widget([
                      'options' => ['id' => "nav-mobile", 'class' => 'side-nav'],
                      'items' => $menuItems,
                      'linkTemplate' => '<a href="{url}" class="waves-effect waves-light">{label}</a>'
                  ]);
                  echo Menu::widget([
                      'options' => ['class' => 'right hide-on-med-and-down'],
                      'items' => $menuItems,
                      'linkTemplate' => '<a href="{url}" class="waves-effect waves-light">{label}</a>'
                  ]);
              ?>
              <a href="#" data-activates="nav-mobile" class="button-collapse waves-effect waves-light"><i class="material-icons">menu</i></a>
            </div>
        </nav>
    </div>
    <main>
    <div class="container">
      <div class="section">
        <div class="row">
          <div class="col s12 m12">
            <?php echo $content; ?>
          </div>
        </div>
      </div>
    </div>
    </main>
    <div class="fixed-action-btn vertical click-to-toggle" style="bottom: 45px; right: 24px;">
        <a class="btn-floating btn-large green darken-3">
            <i class="large material-icons">view_list</i>
        </a>
        <?php if (!Yii::$app->user->isGuest) { ?>
        <ul>
            <li><a class="btn-floating modal-trigger red tooltipped" data-position="bottom" data-delay="50" data-tooltip="Submit PESSTA+" href="#modal1"><i class="material-icons">file_upload</i></a></li>
            <li><a class="btn-floating modal-trigger yellow darken-1 tooltipped" data-position="bottom" data-delay="50" data-tooltip="My PESSTA+" href="#modal4"><i class="material-icons">attachment</i></a></li>
            <li><a class="btn-floating modal-trigger green tooltipped" data-position="bottom" data-delay="50" data-tooltip="Cek Validasi" href="<?=  Yii::$app->getUrlManager()->createUrl(['/validasi'])?>"><i class="material-icons">school</i></a></li>
            <li><a class="btn-floating modal-trigger blue tooltipped" data-position="bottom" data-delay="50" data-tooltip="Kelola PESSTA+" href="#modal3"><i class="material-icons">notifications</i></a></li>
            <li><a class="btn-floating pink tooltipped" data-position="bottom" data-delay="50" data-tooltip="Viewboard PESSTA+" href="#"><i class="material-icons">trending_up</i></a></li>
        </ul>
        <?php } else { ?>
        <ul>
            <li><a class="btn-floating pink tooltipped" data-position="bottom" data-delay="50" data-tooltip="Viewboard PESSTA+" href="#"><i class="material-icons">trending_up</i></a></li>
        </ul>
        <?php } ?>
    </div>
  <footer class="page-footer green">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">About PESSTA+</h5>
          <p class="grey-text text-lighten-4" align="justify"><strong>PESSTA+</strong> adalah sebuah sistem validasi jurnal dan hibah yang dilakukan oleh mahasiswa secara online dan mandiri. Guna membuktikan bahwa mahasiswa atau Pribadi Raharja telah membuat sebuah artikel ilmiah yang lolos atau telah terbit di jurnal yang diakui dalam <u><strong><a class="white-text" href="skptr.ilearning.me/2016/08/21/821sk-ketentuan-jurnalptviii2016-tentang-ketentuan-jurnal-ilmiah-yang-diakui-untuk-penilaian-objektif-di-perguruan-tinggi-raharja" target="_blank">SK No 821 Tentang Ketentuan Jurnal Ilmiah Yang Diakui Untuk Penilaian Objektif Di Perguruan Tinggi Raharja</a></strong></u> maka perlu dilakukan validasi jurnal di PESSTA+, begitu juga untuk membuktikan nilai hibah mahasiswa.</p>
        </div>
        <div class="col l3 s12">
          <h5 class="white-text">Pranala Cepat</h5>
          <ul>
            <li><a class="white-text" href="http://hibah.ilearning.me/" target="_blank">iMe Hibah</a></li>
            <li><a class="white-text" href="<?=Yii::$app->getUrlManager()->createUrl(['api'])?>" target="_blank">API</a></li>
            <!--li><a class="white-text" href="#!">TOS</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li-->
          </ul>
        </div>
        <div class="col l3 s12">
          <h5 class="white-text">How can we help?</h5>
          <ul align="center">
            <li class="center"><a id="mibew-agent-button" href="http://iduhelp.magics.co/chat?locale=en" target="_blank"><img src="http://iduhelp.magics.co/b?i=mibew&amp;lang=en" border="0" alt=""></li>
            <li class="center"><a href="http://iran.ilearning.me/2015/02/04/etalase-iran/" target="_blank"><img class="aligncenter size-full wp-image-55" alt="iran" src="https://lh6.googleusercontent.com/-c1xLfkA4Im8/Vw8Pn3EsJiI/AAAAAAAAEW0/JABK3XovRosfirKVlEQrwzHy8x0EhDG5ACL0B/w432-h216-no/Logo%2BiRAN%2B3.png" width="210px" height="80px"></a></li>
            <li><a class="white-text" href="https://plus.google.com/u/0/communities/117216974662560898299?prsrc=3" target="_blank" rel="publisher" target="_top" style="text-decoration:none;display:inline-block;color:#333;text-align:center; font:13px/16px arial,sans-serif;white-space:nowrap;">
                <span style="display:inline-block;font-weight:bold;vertical-align:top;margin-right:5px; margin-top:8px;">PESSTA+</span><span style="display:inline-block;vertical-align:top;margin-right:15px; margin-top:8px;">on</span>
                <img src="//ssl.gstatic.com/images/icons/gplus-32.png" alt="Google+" style="border:0;width:32px;height:32px;"/> </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">          
      Made with 
        <?= Html::tag('i', 'favorite', ['class'=>'tiny material-icons red-text']) ?> by
        <?= Html::a('PESSTA+', 'http://widuri.raharja.info/index.php?title=PESSTA%2B', ['class'=>'green-text text-lighten-4', 'target'=>'_blank']) ?>.
          <!-- Start of StatCounter Code for Default Guide -->
            <script type="text/javascript">
            var sc_project=10927023; 
            var sc_invisible=0; 
            var sc_security="f8d531de"; 
            var scJsHost = (("https:" == document.location.protocol) ?
            "https://secure." : "http://www.");
            document.write("<sc"+"ript type='text/javascript' src='" +
            scJsHost+
            "statcounter.com/counter/counter.js'></"+"script>");
            </script>
            <noscript><div class="statcounter"><a title="shopify site
            analytics" href="http://statcounter.com/shopify/"
            target="_blank"><img class="statcounter"
            src="//c.statcounter.com/10927023/0/f8d531de/0/"
            alt="shopify site analytics"></a></div></noscript>
          <!-- End of StatCounter Code for Default Guide -->
        <?= Html::a('unique visitors.', 'http://statcounter.com/p10927023/?guest=1', ['class'=>'grey-text text-lighten-4', 'target'=>'_blank']) ?>
        <?= Html::a('Perguruan Tinggi Raharja', 'http://www.raharja.ac.id', ['class'=>'grey-text text-lighten-4 right', 'target'=>'_blank']) ?>
      </div>
    </div>
  </footer>
  <?php $this->registerJs("(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-76850300-1', 'auto');
  ga('send', 'pageview');", View::POS_END, 'my-analytic'); ?>  
  <?php Spaceless::end(); ?>
  <?php $this->endBody() ?>
  </body>
</html>
<?php $this->endPage(); ?>