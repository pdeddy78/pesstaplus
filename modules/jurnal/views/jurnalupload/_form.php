<?php

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

$this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/easy-autocomplete.min.css');
$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/jquery.easy-autocomplete.min.js');
$this->registerAssetBundle(yii\web\JqueryAsset::className(), View::POS_HEAD);
/* @var $this yii\web\View */
/* @var $model app\modules\jurnal\models\JurnalUpload */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jurnal-upload-form">

    <?php $form = ActiveForm::begin(['options' => [
                                        'enctype' => 'multipart/form-data',
                                        'class' => 'col s12 l8'
                                    ]]); 
    ?>
    <span class="red-text text-darken-4"><?= $form->errorSummary($model, ['class'=>'card-panel red lighten-5']); ?></span>

    <?= $form->field($model, 'NIPR')->hiddenInput(['maxlength' => true, 'readonly' => true, 'value' => Yii::$app->user->identity->NIPR])->label(false) ?>
    <?= $form->field($model, 'NamaLengkap')->textInput(['maxlength' => true, 'readonly' => true, 'value' => Yii::$app->user->identity->NamaLengkap]) ?>
    <?= $form->field($model, 'AlamatEmail')->textInput(['maxlength' => true, 'readonly' => true, 'value' => Yii::$app->user->identity->AlamatEmail]) ?>
    <?= $form->field($model, 'JudulPaper')->textInput(['maxlength' => true, 'autofocus' => true, 'class'=>'validate'])->hint('Provide your hint here')->label(null, ['title' => 'Judul Paper adalah Judul dari Paper yang telah Anda buat.','data-error'=>'wrong','data-success'=>'right']) ?>
    <?php // echo $form->field($model, 'BerkasPaper')->fileInput(['maxlength' => true, 'class'=>'file-path-wrapper validate']) ?>
    <?= $form->field($model, 'BerkasPaper')->widget(FileInput::classname(), [        
        'options'=>[
            'accept'=>'file/*',
            'required'=>'required',
            'aria-required'=>'true',
        ],
        'pluginOptions'=>[
            'allowedFileExtensions'=>['pdf'],
            'maxFileSize'=>1024*1024*7
        ]
    ])->hint('Provide your hint here');?>
    <?= $form->field($model, 'UrlPaper')->textInput(['class'=>'validate','placeholder'=>'http://'])->label(null, ['data-error'=>'wrong','data-success'=>'right']); ?>
    <?= $form->field($model, 'GambarPaper')->textInput(['class'=>'validate','placeholder'=>'http://'])->label(null, ['data-error'=>'wrong','data-success'=>'right']) ?>
    <?= $form->field($model, 'NamaJurnal')->textInput(['autofocus' => true, 
                                                        'id'=>'provider-remote',
                                                        'required'=>'required',
                                                        'aria-required'=>'true',
                                                        'placeholder'=>'Silakan pilih tempat jurnal diterbitkan...',
                                                        'autocomplete'=>'off'])->hint('Sesuai dengan SK No 821 Tentang Ketentuan Jurnal Ilmiah Yang Diakui Untuk Penilaian Objektif Di Perguruan Tinggi Raharja '.Html::a('di sini', 'http://127.0.0.1/yii2/pesstaplus/jurnal/jurnalUpload/skptr.ilearning.me/2016/08/21/821sk-ketentuan-jurnalptviii2016-tentang-ketentuan-jurnal-ilmiah-yang-diakui-untuk-penilaian-objektif-di-perguruan-tinggi-raharja', ['target'=>'_blank'])); 
    ?>
    <?php                     
        $this->registerJs(
            'var options = {
                    url: function(phrase) {
                        return "//127.0.0.1/yii2/pesstaplus/api/jurnalMaster";
                    },
                    
                    getValue: "NamaJurnal",
                    requestDelay: 500,
                    template: {
                        type: "description",
                        fields: {
                            description: "Penerbit"
                        }
                    },
                    list: {
                        maxNumberOfElements: 4,
                        match: {
                                enabled: true
                        },
                        sort: {
                                enabled: false
                        },
                        showAnimation: {
                                type: "fade",
                                time: 400,
                                callback: function() {}
                        },
                        hideAnimation: {
                                type: "slide",
                                time: 400,
                                callback: function() {}
                        }
                    }
            };
            $("#provider-remote").easyAutocomplete(options);', View::POS_END, 'provider-remote');  
     ?>
    <?php /*
    <?= $form->field($model, 'IDJurnal', ['labelOptions' => ['data-error'=>'wrong','data-success'=>'right']])->textInput() ?>
    <?= $form->field($model, 'TanggalSubmit')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'TanggalAcc')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'StatusPaper')->dropDownList([ '0', '1', '2', ], ['prompt' => '']) ?>
    <?= $form->field($model, 'UpdatedBy')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'Details')->textarea(['rows' => 6]) ?>
    */ ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'waves-effect waves-light btn green' : 'waves-effect waves-light btn green']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
