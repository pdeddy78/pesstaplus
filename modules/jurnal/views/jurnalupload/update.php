<?php

use yii\helpers\Html;
//use yii\web\View;
use kartik\form\ActiveForm;
//use yii\widgets\ActiveForm;

/* @var $model app\modules\jurnal\models\JurnalUpload */

$this->title = 'Status Jurnal Upload  #' . $model->JurnalSubmit;
$this->params['breadcrumbs'][] = ['label' => 'Jurnal Uploads', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->JurnalSubmit, 'url' => ['view', 'id' => $model->JurnalSubmit]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jurnal-upload-update">

    <h2><?= Html::encode($this->title) ?></h2>
    <?php $form = ActiveForm::begin(['options' => [
                                        'class' => 'col s12 l8'
                                    ]]); 
    ?>
    <span class="red-text text-darken-4"><?= $form->errorSummary($model, ['class'=>'card-panel red lighten-5']); ?></span>
    
    <?= $form->field($model, 'StatusPaper')->radioButtonGroup([0 => 'Tidak Valid', 2 => 'Proses', 1 => 'Valid'],[
                                                                //'class' => 'btn-group-sm',
                                                                'disabledItems'=>[2],
                                                                'itemOptions' => ['labelOptions' => ['class' => 'waves-effect waves-light btn btn-success']]
                                                            ])->hint('Silakan pilih Status dengan mengklik salah satunya (Tidak Valid/Valid).'); 
    ?>
    
    <?= $form->field($model, 'Details')->widget(\vova07\imperavi\Widget::className(), [
        'settings' => [
                'lang' => 'id',
                'minHeight' => 200,
                'plugins' => [
                    'table',
                    'video',
                    'clips',
                    'fullscreen'
                ]
            ]
    ])?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'waves-effect waves-light btn green' : 'waves-effect waves-light btn green']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
