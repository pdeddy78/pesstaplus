<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\jurnal\models\JurnalUpload */

$this->title = 'Create Jurnal Upload';
$this->params['breadcrumbs'][] = ['label' => 'Jurnal Uploads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jurnal-upload-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
