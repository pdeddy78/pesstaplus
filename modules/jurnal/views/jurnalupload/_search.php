<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\jurnal\models\JurnalUploadSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jurnal-upload-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'JurnalSubmit') ?>

    <?= $form->field($model, 'NIPR') ?>

    <?= $form->field($model, 'NamaLengkap') ?>

    <?= $form->field($model, 'AlamatEmail') ?>

    <?= $form->field($model, 'JudulPaper') ?>

    <?php // echo $form->field($model, 'BerkasPaper') ?>

    <?php // echo $form->field($model, 'UrlPaper') ?>

    <?php // echo $form->field($model, 'GambarPaper') ?>

    <?php // echo $form->field($model, 'IDJurnal') ?>

    <?php // echo $form->field($model, 'TanggalSubmit') ?>

    <?php // echo $form->field($model, 'TanggalAcc') ?>

    <?php // echo $form->field($model, 'StatusPaper') ?>

    <?php // echo $form->field($model, 'UpdatedBy') ?>

    <?php // echo $form->field($model, 'Details') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
