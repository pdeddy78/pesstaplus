<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\jurnal\models\JurnalUpload */
/* */
$this->title = 'Jurnal Upload #'.$model->JurnalSubmit;
$this->params['breadcrumbs'][] = ['label' => 'Jurnal Uploads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jurnal-upload-view">

    <h2><?= Html::encode($this->title) ?></h2>
<?php
if(\Yii::$app->user->can('SpecialPermission')) {
?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'NIPR',
            'NamaLengkap',
            //'AlamatEmail:email',
            'JudulPaper',
            [
                'attribute' => 'BerkasPaper',
                'value' => Html::a('Lihat Paper di sini', 'https://docs.google.com/gview?url=http://pesstaplus.raharja.me'.$model->BerkasPaper, ['target' => '_blank']),
                'format' => 'raw',
            ],
            [
                'attribute' => 'UrlPaper',
                'value' => $model->UrlPaper != null ? Html::a('Lihat Paper Online di sini', $model->UrlPaper, ['target'=>'_blank']) : '<i>Tidak Ada</i>',
                'format' => 'raw',
            ],
            [
                'attribute' => 'GambarPaper',
                'value' => Html::img($model->GambarPaper, ['class'=>'responsive-img z-depth-1']),
                'format' => 'raw',
            ],
            'jurnal.NamaJurnal',
            [
                'attribute' => 'StatusPaper',
                'value' => (($model->StatusPaper ==0) ? Html::a('Tidak Valid', ['update', 'id' => $model->JurnalSubmit], ['class'=>'red-text text-darken-2']) : (($model->StatusPaper ==1)? Html::a('Valid', ['update', 'id' => $model->JurnalSubmit], ['class'=>'teal-text text-darken-1']) : Html::a('Belum Proses', ['update', 'id' => $model->JurnalSubmit], ['class'=>'lime-text text-darken-1']))),
                'format' => 'raw',
            ],
            [
                'label' => 'Tanggal Submit',
                'format' => 'raw',
                'value' => \davidhirtz\yii2\timeago\Timeago::tag($model['TanggalSubmit']),
            ],
            [
                'label' => 'Tanggal Acc',
                'format' => 'raw',
                'value' => \davidhirtz\yii2\timeago\Timeago::tag($model['TanggalAcc']),
            ],
            'UpdatedBy',
            [
                'label' => 'Details',
                'format' => 'html',
                'value' => $model->Details,
            ],
        ],
    ]) ?>
<?php
//if user can manage journal
} elseif(\Yii::$app->user->can('ManageJurnal')) {
?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'NIPR',
            'NamaLengkap',
            //'AlamatEmail:email',
            'JudulPaper',
            [
                'attribute' => 'BerkasPaper',
                'value' => Html::a('Lihat Paper di sini', 'https://docs.google.com/gview?url=http://pesstaplus.raharja.me'.$model->BerkasPaper, ['target' => '_blank']),
                'format' => 'raw',
            ],
            [
                'attribute' => 'UrlPaper',
                'value' => $model->UrlPaper != null ? Html::a('Lihat Paper Online di sini', $model->UrlPaper, ['target'=>'_blank']) : '<i>Tidak Ada</i>',
                'format' => 'raw',
            ],
            [
                'attribute' => 'GambarPaper',
                'value' => Html::img($model->GambarPaper, ['class'=>'responsive-img z-depth-1']),
                'format' => 'raw',
            ],
            'jurnal.NamaJurnal',
            [
                'attribute' => 'StatusPaper',
                'value' => (($model->StatusPaper ==0) ? "Tidak Valid": (($model->StatusPaper ==1)? "Valid" : Html::a('Belum Proses', ['update', 'id' => $model->JurnalSubmit], ['class'=>'lime-text text-darken-1']))),
                'format' => 'raw',
            ],
            [
                'label' => 'Tanggal Submit',
                'format' => 'raw',
                'value' => \davidhirtz\yii2\timeago\Timeago::tag($model['TanggalSubmit']),
            ],
            [
                'label' => 'Details',
                'format' => 'html',
                'value' => $model->Details,
            ],
        ],
    ]) ?>
<?php } else { ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'NamaLengkap',
            'JudulPaper',
            [
                'attribute' => 'BerkasPaper',
                'value' => Html::a('Lihat Paper di sini', 'https://docs.google.com/gview?url=http://pesstaplus.raharja.me'.$model->BerkasPaper, ['target' => '_blank']),
                'format' => 'raw',
            ],
            [
                'attribute' => 'UrlPaper',
                'value' => $model->UrlPaper != null ? Html::a('Lihat Paper Online di sini', $model->UrlPaper, ['target'=>'_blank']) : '<i>Tidak Ada</i>',
                'format' => 'raw',
            ],
            [
                'attribute' => 'GambarPaper',
                'value' => Html::img($model->GambarPaper, ['class'=>'responsive-img z-depth-1']),
                'format' => 'raw',
            ],
            'jurnal.NamaJurnal',
            [
                'attribute' => 'StatusPaper',
                'value' => (($model->StatusPaper ==0) ? "Tidak Valid": (($model->StatusPaper ==1)? "Valid" : "<span class='lime-text text-darken-1'><b>Belum Proses</b></span>")),
                'format' => 'raw',
            ],
            [
                'label' => 'Tanggal Submit',
                'format' => 'raw',
                'value' => \davidhirtz\yii2\timeago\Timeago::tag($model['TanggalSubmit']),
            ],
            [
                'label' => 'Details',
                'format' => 'html',
                'value' => $model->Details,
            ],
        ],
    ]); } ?>
</div>