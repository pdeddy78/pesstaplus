<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\jurnal\models\JurnalUpload */

$this->title = 'Submit Jurnal';
$this->params['breadcrumbs'][] = ['label' => 'Jurnal Upload', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jurnal-upload-submit">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
