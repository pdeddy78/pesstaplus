<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\jurnal\models\JurnalMaster */

$this->title = 'Update Jurnal Master: ' . $model->IDJurnal;
$this->params['breadcrumbs'][] = ['label' => 'Jurnal Masters', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->IDJurnal, 'url' => ['view', 'id' => $model->IDJurnal]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jurnal-master-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
