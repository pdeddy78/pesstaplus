<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\jurnal\models\JurnalMaster */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jurnal-master-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'NamaJurnal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'AlamatJurnal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'NomorSeri')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Penerbit')->textInput(['maxlength' => true]) ?>

    <?php // $form->field($model, 'UpdatedBy')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Tambah' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
