<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\jurnal\models\JurnalMaster */

$this->title = 'Tambah Master Jurnal';
$this->params['breadcrumbs'][] = ['label' => 'Jurnal Masters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jurnal-master-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
