<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\jurnal\models\JurnalMaster */

$this->title = $model->IDJurnal;
$this->params['breadcrumbs'][] = ['label' => 'Jurnal Masters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jurnal-master-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->IDJurnal], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->IDJurnal], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'IDJurnal',
            'NamaJurnal',
            'AlamatJurnal',
            'NomorSeri',
            'Penerbit',
            'UpdatedBy',
        ],
    ]) ?>

</div>
