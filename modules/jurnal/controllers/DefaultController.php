<?php

namespace app\modules\jurnal\controllers;
use Yii;
/**
 * Default controller for the `jurnal` module
 */
class DefaultController extends \app\components\BaseController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        if(\Yii::$app->user->can('ManageJurnal'))
        {
            return $this->render('index');
        }
        else die("mati");
    }
    
    public function actionMail()
    {
        $email = 'deddy.pratama@raharja.info';
        $subject='This is tes mail...';
        $from='Deddy';
        $view='views/jurnal/toSuccess';
        $model=null;
        $send = \app\controllers\MailController::sendMail($email, $subject, $from, $view, $model);
        if($send) die('Its work!');
    }

}
