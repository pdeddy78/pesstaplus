<?php

namespace app\modules\jurnal\controllers;

use Yii;
use app\modules\jurnal\models\JurnalUpload;
use app\modules\jurnal\models\JurnalUploadSearch;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\UploadedFile;
use yii\db\Transaction;
use yii\helpers\FileHelper;
//use yii\filters\VerbFilter;

/**
 * JurnalUploadController implements the CRUD actions for JurnalUpload model.
 */
class JurnalUploadController extends \app\components\BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            /*
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
             * 
             */
        ];
    }

    /**
     * Lists all JurnalUpload models.
     * @return mixed
     */
    public function actionIndex()
    {
        // \Yii::$app->getSession()->setFlash('info', 'Ini halaman jurnalUpload..');
        if(\Yii::$app->user->can('ManageJurnal'))
        {
            $searchModel = new JurnalUploadSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->pagination->pageSize=10;
            $dataProvider->query->andWhere(['AlamatEmail'=>Yii::$app->user->identity->AlamatEmail]);
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        else throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
    }
    
    public function actionAdmin()
    {
        if(\Yii::$app->user->can('ManageJurnal'))
        {
            $searchModel = new JurnalUploadSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->pagination->pageSize=10;
            $dataProvider->query->andWhere(['StatusPaper'=>'2']);
            
            return $this->render('admin', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        else throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
    }

    /**
     * Displays a single JurnalUpload model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new JurnalUpload model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
    
    public function actionCreate()
    {
        $model = new JurnalUpload();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->JurnalSubmit]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionUpload()
    {
        $model = new JurnalUpload();

        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $BerkasPaper = UploadedFile::getInstance($model, 'BerkasPaper');
                $ext = end((explode(".", $BerkasPaper->name)));
                // generate a unique file name to prevent duplicate filenames
                $model->BerkasPaper = Yii::$app->security->generateRandomString().".{$ext}";
                if (!is_null($BerkasPaper)) {
                    if ($model->save()) {
                        // the path to save file, you can set an uploadPath                    
                        $path = Yii::getAlias('@app').'/uploads/jurnal/'.date('Y').'/'.  date('m') .'/';
                        FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
                        $save = $BerkasPaper->saveAs($path.'Jurnal_RHJ_'.$model->JurnalSubmit.'.'.$ext);
                        return $this->redirect(['view', 'id' => $model->JurnalSubmit]);             
                    }  else {
                        var_dump ($model->getErrors()); die();
                    }    
                }
                
            } catch (\Exception $e) {                
                $transaction->rollBack();
                //throw $e;
                die(var_dump($e->getMessage()));
            }
            
        }
        return $this->render('submit', [
            'model' => $model
        ]);
    }
    */
    public function actionSubmit()
    {
        $model = new JurnalUpload();

        if ($model->load(Yii::$app->request->post())) 
        {            
            $transaction = Yii::$app->db->beginTransaction();
            try {                
                $messageType="warning";
                $message = "There are some errors ";
                $uploadBerkas = UploadedFile::getInstance($model, 'BerkasPaper');
                $extUploadBerkas = end((explode(".", $uploadBerkas->name)));
                $model->BerkasPaper = Yii::$app->security->generateRandomString().".{$extUploadBerkas}"; //tempName
                if($uploadBerkas->size < 7*1024*1024)
                {                    
                    if($model->save())
                    {                    
                        $messageType = 'success';
                        $message = "Well done! You successfully submit jurnal ";
                        $model2 = JurnalUpload::findOne($model->JurnalSubmit);
                        if(!empty($uploadBerkas)) 
                        {
                            $path = Yii::getAlias('@app').'/uploads/jurnal/'.date('Y').'/'.  date('m') ;
                            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
                            if($uploadBerkas->saveAs($path.'/Jurnal_RHJ_'.$model2->JurnalSubmit.'.'.$extUploadBerkas))
                            {
                                \Yii::$app->db->createCommand("UPDATE {{%TTMahasiswaJurnal}} SET BerkasPaper=:BerkasPaper WHERE JurnalSubmit=:JurnalSubmit")
                                ->bindValue(':BerkasPaper', '/uploads/jurnal/'.date('Y').'/'.  date('m').'/Jurnal_RHJ_'.$model2->JurnalSubmit.'.'.$extUploadBerkas)
                                ->bindValue(':JurnalSubmit', $model2->JurnalSubmit)
                                ->execute();
                                /*
                                if ($mail->send() && $mail2->send()) 
                                {
                                        $message .= 'and file uploaded. We will respond to you as soon as possible.';
                                } else {
                                        $message .= 'and file uploaded but error while sending email.';
                                } */
                            }
                            else 
                            {
                                //var_dump ($uploadBerkas); die();
                                $messageType = 'warning';
                                $message .= 'but file not uploaded';
                            }
                        }
                        $transaction->commit();
                        Yii::$app->session->setFlash($messageType, $message);
                        $this->redirect(['view', 'id' => $model->JurnalSubmit]);
                    }
                    else {
                        //var_dump ($model->getErrors()); die();
                        $errors = $model->errors;
                        Yii::$app->session->setFlash('error', 'Galat dalam menyimpan PDF');
                        //$this->refresh();
                        return $this->render('submit', [
                            'model' => $model,
                        ]);
                    }  
                }
                else {
                    //var_dump ($model->getErrors()); die();
                    Yii::$app->session->setFlash('error', 'Berkas PDF terlalu besar');
                    $this->refresh();
                }
            } catch (\Exception $e) {
                
                $transaction->rollBack();
                //throw $e;
                die(var_dump($e->getMessage()));
                Yii::$app->session->setFlash('error', "{$e->getMessage()}");
                //$this->refresh();
            }
        } else {            
            return $this->render('submit', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing JurnalUpload model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->JurnalSubmit]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing JurnalUpload model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the JurnalUpload model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return JurnalUpload the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = JurnalUpload::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }    
}