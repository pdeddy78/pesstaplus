<?php

namespace app\modules\jurnal\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
/**
 * This is the model class for table "ttmahasiswajurnal".
 *
 * @property integer $JurnalSubmit
 * @property string $NIPR
 * @property string $NamaLengkap
 * @property string $AlamatEmail
 * @property string $JudulPaper
 * @property string $BerkasPaper
 * @property string $UrlPaper
 * @property string $GambarPaper
 * @property integer $IDJurnal
 * @property string $TanggalSubmit
 * @property string $TanggalAcc
 * @property string $StatusPaper
 * @property string $UpdatedBy
 * @property string $Details
 */
class JurnalUpload extends \yii\db\ActiveRecord
{
    public $NamaJurnal;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%TTMahasiswaJurnal}}';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NamaLengkap', 'NamaJurnal', 'AlamatEmail', 'JudulPaper', 'BerkasPaper', 'GambarPaper'], 'required'],
            [['UrlPaper', 'GambarPaper', 'BerkasPaper', 'StatusPaper', 'Details', 'NamaJurnal'], 'string'],
            [['IDJurnal', 'TanggalSubmit', 'TanggalAcc'], 'integer'],
            [['NIPR'], 'string', 'max' => 14],            
            ['JudulPaper', 'isExist'],
            [['NamaLengkap'], 'string', 'max' => 200],
            [['AlamatEmail'], 'string', 'max' => 100],
            [['JudulPaper', 'BerkasPaper'], 'string', 'max' => 255],
            [['UpdatedBy'], 'string', 'max' => 20],
            [['GambarPaper', 'UrlPaper'], 'url','defaultScheme' => 'http'], //Prepend "http://" to the "website" attribute
            [['StatusPaper'],'default','value'=>'2'],
            [['NamaJurnal','Details'], 'safe'],
            //['IDJurnal', 'exist', 'targetClass' => '\app\modules\jurnal\models\JurnalMaster', 'targetAttribute' => 'IDJurnal'] ,
            ['BerkasPaper', 'file', 'extensions' => 'pdf', 
                                    //'checkExtensionByMimeType'=>false, 
                                    'mimeTypes' => 'application/pdf',
                                    'maxSize' => 1024*1024*7,
                                    'maxFiles' => 1,
                                    'tooBig'=>'Ukuran berkas harus lebih kecil dari 8MB', 
                                    'skipOnEmpty' => false, 
                                    'on' => 'insert'],
        ];
    }
    
    public function isExist($attribute, $params) {
        $cek = JurnalUpload::findOne(['AlamatEmail' => $this->AlamatEmail, 'JudulPaper' => $this->JudulPaper, 'StatusPaper' => '2']);
        if($cek)
        {
            $this->addError($attribute, 'Maaf, kamu sudah melakukan submit jurnal dengan judul ini. Silakan tunggu hingga selesai dilakukan validasi.');
        }
        $cek = JurnalUpload::findOne(['AlamatEmail' => $this->AlamatEmail, 'JudulPaper' => $this->JudulPaper, 'StatusPaper' => '1']);
        if($cek)
        {
            $this->addError($attribute, 'Maaf, jurnal yang kamu upload dengan judul ini sudah dinyatakan valid. Silakan submit dengan judul yang berbeda.');
        }
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'JurnalSubmit' => 'Jurnal Submit',
            'NIPR' => 'NIPR',
            'NamaLengkap' => 'Nama Lengkap',
            'AlamatEmail' => 'Alamat Email',
            'JudulPaper' => 'Judul Paper',
            'BerkasPaper' => 'Berkas Paper',
            'UrlPaper' => 'Url Paper (jika ada)',
            'GambarPaper' => 'Cover Paper',
            'IDJurnal' => 'ID Jurnal',
            'TanggalSubmit' => 'Tanggal Submit',
            'TanggalAcc' => 'Tanggal Acc',
            'StatusPaper' => 'Status Paper',
            'UpdatedBy' => 'Updated By',
            'Details' => 'Details',
        ];
    }
    /*
    public function upload()
    {
        $BerkasPaper = UploadedFile::getInstance($this, 'BerkasPaper');
        if (empty($BerkasPaper)) {
            return false;
        }
        if ($this->validate()) {
            
            $extUploadBerkas = end((explode(".", $BerkasPaper->name)));
            $path = Yii::getAlias('@app').'/uploads/jurnal/'.date('Y').'/'.  date('m') ;
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $pathName = $path.'/Jurnal_RHJ_'.$this->JurnalSubmit.'.'.$extUploadBerkas;
            
            $this->BerkasPaper = $pathName;
            return $this->BerkasPaper;
        } else {
            return false;
        }
    }
    
    /*
    public function upload()
    {
        if ($this->validate()) {
            $path = Yii::getAlias('@app').'/uploads/jurnal/'.date('Y').'/'.  date('m') ;
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $this->BerkasPaper->saveAs($path . $this->BerkasPaper->baseName . '.' . $this->BerkasPaper->extension);
            return true;
        } else {
            return false;
        }
    }
     
     */
    public function getJurnal()
    {
        return $this->hasOne(JurnalMaster::className(), ['IDJurnal' => 'IDJurnal']);
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
        // Place your custom code here           
            $Jurnal = JurnalMaster::findOne([
                'NamaJurnal' => $this->NamaJurnal,
            ]);
            $this->NIPR = trim($this->NIPR);
            $this->JudulPaper = strtoupper($this->JudulPaper);
            $this->TanggalSubmit= time();
            $this->IDJurnal = $Jurnal->IDJurnal;           
            return true;
        } 
        else 
        {
            return false;
        }
    }
}
