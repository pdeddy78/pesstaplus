<?php

namespace app\modules\jurnal\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\jurnal\models\JurnalMaster;

/**
 * JurnalMasterSearch represents the model behind the search form about `app\modules\jurnal\models\JurnalMaster`.
 */
class JurnalMasterSearch extends JurnalMaster
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IDJurnal'], 'integer'],
            [['NamaJurnal', 'AlamatJurnal', 'NomorSeri', 'Penerbit', 'UpdatedBy'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JurnalMaster::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'IDJurnal' => $this->IDJurnal,
        ]);

        $query->andFilterWhere(['like', 'NamaJurnal', $this->NamaJurnal])
            ->andFilterWhere(['like', 'AlamatJurnal', $this->AlamatJurnal])
            ->andFilterWhere(['like', 'NomorSeri', $this->NomorSeri])
            ->andFilterWhere(['like', 'Penerbit', $this->Penerbit])
            ->andFilterWhere(['like', 'UpdatedBy', $this->UpdatedBy]);

        return $dataProvider;
    }
}
