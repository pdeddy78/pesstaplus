<?php

namespace app\modules\jurnal\models;
use Yii;

/**
 * This is the model class for table "tmjurnal".
 *
 * @property integer $IDJurnal
 * @property string $NamaJurnal
 * @property string $AlamatJurnal
 * @property string $NomorSeri
 * @property string $Penerbit
 * @property string $UpdatedBy
 */
class JurnalMaster extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%TMJurnal}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NamaJurnal', 'NomorSeri', 'Penerbit', 'UpdatedBy'], 'required'],
            [['NamaJurnal'], 'string', 'max' => 100],
            [['AlamatJurnal', 'Penerbit'], 'string', 'max' => 255],            
            [['AlamatJurnal'], 'url'],
            [['NomorSeri'], 'string', 'max' => 50],
            [['UpdatedBy'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IDJurnal' => 'Idjurnal',
            'NamaJurnal' => 'Nama Jurnal',
            'AlamatJurnal' => 'Alamat Jurnal',
            'NomorSeri' => 'Nomor Seri',
            'Penerbit' => 'Penerbit',
            'UpdatedBy' => 'Updated By',
        ];
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) 
        {        
            $this->UpdatedBy = Yii::$app->user->identity->Username;
            return true;
        } 
        else 
        {
            return false;
        }
    }
}
