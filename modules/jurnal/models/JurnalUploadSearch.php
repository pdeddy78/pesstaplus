<?php

namespace app\modules\jurnal\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\jurnal\models\JurnalUpload;

/**
 * JurnalUploadSearch represents the model behind the search form about `app\modules\jurnal\models\JurnalUpload`.
 */
class JurnalUploadSearch extends JurnalUpload
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['JurnalSubmit', 'TanggalSubmit', 'TanggalAcc'], 'integer'],
            [['NIPR', 'NamaLengkap', 'AlamatEmail', 'JudulPaper', 'BerkasPaper', 'UrlPaper', 'GambarPaper', 'IDJurnal', 'StatusPaper', 'UpdatedBy', 'Details', 'TanggalSubmit', 'TanggalAcc'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JurnalUpload::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
      
        // grid filtering conditions
        $query->andFilterWhere([
            'JurnalSubmit' => $this->JurnalSubmit,
            'TanggalSubmit' => $this->TanggalSubmit,
            //'DATE(FROM_UNIXTIME('.$this->tableName() . '.TanggalSubmit))' => $this->TanggalSubmit,
            'TanggalAcc' => $this->TanggalAcc,
        ]);

        $query->andFilterWhere(['like', 'NIPR', $this->NIPR])
            ->andFilterWhere(['like', 'NamaLengkap', $this->NamaLengkap])
            ->andFilterWhere(['like', 'AlamatEmail', $this->AlamatEmail])
            ->andFilterWhere(['like', 'JudulPaper', $this->JudulPaper])
            ->andFilterWhere(['like', 'BerkasPaper', $this->BerkasPaper])
            ->andFilterWhere(['like', 'UrlPaper', $this->UrlPaper])
            ->andFilterWhere(['like', 'GambarPaper', $this->GambarPaper])
            ->andFilterWhere(['like', 'IDJurnal', $this->IDJurnal])
            ->andFilterWhere(['like', 'StatusPaper', $this->StatusPaper])
            ->andFilterWhere(['like', 'UpdatedBy', $this->UpdatedBy])
            ->andFilterWhere(['like', 'Details', $this->Details]);

        return $dataProvider;
    }
    
    public function getTanggalAcc()
    {
        return $this->TanggalAcc ? date('d-m-Y', $this->TanggalAcc) : null;
    }
}
