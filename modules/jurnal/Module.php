<?php

namespace app\modules\jurnal;

/**
 * jurnal module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\jurnal\controllers';
    public $defaultRoute = 'jurnalMaster';
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}
