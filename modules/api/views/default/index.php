<?php
/* @var $this DefaultController */
use yii\helpers\Html;

$this->title='API';
?>
<div class="api-default-index">
    <h2>API</h2>
    PENDAHULUAN<hr>
    <p class="" align="justify">PESSTA+ API (Application Programming Interface) memungkinkan developer mengakses data yang ada di dalam server PESSTA+. 
    Layanan ini disediakan untuk membantu dalam perancangan/pengembangan website atau aplikasi agar dapat berjalan dengan baik di platformnya. 
    Layanan daripada PESSTA+ API ini dapat bebas digunakan dalam banyak cara. Respon yang dikembalikan semuanya berupa format JSON, <a class="black-text" href="#">Why JSON?</a></p>

    LAYANAN<hr>
    <p><strong><a class="black-text" href="#jurnal_list">jurnal.list</a></strong>   &mdash; Lihat daftar nama jurnal yang diakui dalam SK Direktur No. 821.</p>
    <p><strong><a class="black-text" href="#jurnal_layak">jurnal.layak</a></strong>   &mdash; Lihat daftar nama Pribadi Raharja yang memiliki jurnal.</p>

    DOKUMENTASI API<hr>
    <div id="jurnal_list"><p align="justify"> <strong>Daftar Jurnal yang Diakui</strong><br/>
    Mendapatkan data dari daftar SK Direktur No.821 Tentang Kententuan Jurnal Yang Diakui Di Perguruan Tinggi Raharja.<br/>
    <strong>URL: </strong> <?= Html::a('http:/'.Yii::$app->homeUrl.'api/jurnalMaster/index', ['jurnalMaster/index'], ['rel' => 'nofollow','target'=>'_blank']) ?><br/>
    <strong>HTTP method:</strong> GET</p></div>
    <div id="jurnal_layak"><p align="justify"> <strong>Daftar Pribadi Raharja Layak Jurnal</strong><br/>
    Mendapatkan data Pribadi Raharja yang telah dinyatakan valid memiliki jurnal.<br/>
    <strong>URL:</strong> <a href="http://pesstaplus.raharja.me/api/jurnal/layak" rel="nofollow">http://pesstaplus.raharja.me/api/jurnal/layak</a><br/>
    <strong>HTTP method:</strong> GET</p></div>
</div>
