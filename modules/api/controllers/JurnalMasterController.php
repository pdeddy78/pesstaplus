<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace app\modules\api\controllers;
use yii\rest\Controller;
use yii\web\Response; 
use yii\helpers\ArrayHelper;

class JurnalMasterController extends Controller
{
    
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['index'],
                // if in a module, use the following IDs for user actions
                // 'only' => ['user/view', 'user/index']
                'formats' => [
                  'application/json' => Response::FORMAT_JSON,
                 //   'application/xml' => Response::FORMAT_XML,
                ],
                'languages' => [
                  'en',
                ],
            ],
        ]);
    }
    protected function verbs() {
        return [
            'index' => ['GET'],
        ];
    }
    
    public function actionIndex()
    {
        return $JurnalMaster = \app\modules\jurnal\models\JurnalMaster::find()->all();
        /*
        return [
            'results' => $JurnalMaster,
        ];
         * 
         */
    }
}