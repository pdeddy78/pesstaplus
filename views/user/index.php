<?php

use yii\helpers\Html;
use yii\grid\GridView;
\davidhirtz\yii2\timeago\TimeagoAsset::register($this);
/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showFooter'=>true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
           // 'ID',
            'Username',
            'AlamatEmail:email',
            [
                'label' => 'KunciOtentik',
                'format'    => 'raw',
                'attribute' => 'KunciOtentik',
                'filter'    => false,
            ],
            //'Bergabung:datetime',
            [
                'label' => 'Bergabung',
                'format' => 'date',
                'attribute' => 'Bergabung',
                'filter'    => false,
            ],
            [
                'label' => 'Kunjungan',
                'format' => 'raw',
                'value' => function ($data) {
                            return \davidhirtz\yii2\timeago\Timeago::tag($data['Kunjungan']);
                            //return Html::tag('time', Yii::$app->formatter->asDatetime(time()));
                      },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => "{view}",
            ],
        ],
    ]); ?>
</div>
