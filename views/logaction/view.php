<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LogAction */

$this->title = $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Log Actions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-action-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'username',
            'ipaddress',
            [
                'label' => 'logtime',
                'format' => 'raw',
                'value' => \davidhirtz\yii2\timeago\Timeago::tag($model['logtime']),
            ],
            'controller',
            'action',
            'details:ntext',
        ],
    ]) ?>

</div>
