<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\LogActionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Log Actions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-action-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<?php Pjax::begin(['id' => 'myPjax']);?>
<h6>Update terakhir: <?= date('d-M-Y H:i:s', time()) ?></h6>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table responsive-table'
        ],
        'pager' => [
            'activePageCssClass' => 'green',
            'options'=>['class'=>'pagination center'],   // set clas name used in ui list of pagination
            'prevPageLabel' => '<i class="material-icons">keyboard_arrow_left</i>',   // Set the label for the "previous" page button
            'nextPageLabel' => '<i class="material-icons">keyboard_arrow_right</i>',   // Set the label for the "next" page button
            'firstPageLabel'=>'<i class="material-icons">first_page</i>',   // Set the label for the "first" page button
            'lastPageLabel'=>'<i class="material-icons">last_page</i>',    // Set the label for the "last" page button
            'nextPageCssClass'=>'next',    // Set CSS class for the "next" page button
            'prevPageCssClass'=>'prev',    // Set CSS class for the "previous" page button
            'firstPageCssClass'=>'first',    // Set CSS class for the "first" page button
            'lastPageCssClass'=>'last',    // Set CSS class for the "last" page button
            'maxButtonCount'=>7,    // Set maximum number of page buttons that can be displayed 
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'username',
            'ipaddress',
            [
                'label' => 'logtime',
                'format' => 'raw',
                'value' => function ($data) {
                            return \davidhirtz\yii2\timeago\Timeago::tag($data['logtime']);
                      },
            ],
            'controller',
            'action',
            //'details:ntext',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url) {
                        return Html::a(
                            '<i class="material-icons">visibility</i>',
                            $url, 
                            [
                                'title' => 'View',
                                'data-pjax' => '0',
                            ]
                        );
                    },
                ],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?>
</div>