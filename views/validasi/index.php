<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\web\View;
$this->title = "Hasil Validasi";
$this->params['breadcrumbs'][] = $this->title;
?>

<h1 class="header center green-text text-darken-5">Hasil Validasi</h1>
<div class="row">
    <div class="col s9 l9">
        <table class="responsive-table">
            <tbody>
                <tr>
                    <td>NIPR</td>
                    <td><?= Yii::$app->user->identity->NIPR ;?></td>
                </tr>
                <tr>
                    <td>Nama Lengkap</td>
                    <td><?= Yii::$app->user->identity->NamaLengkap ;?></td>
                </tr>
                <tr>
                    <td>Alamat Email</td>
                    <td><?= Yii::$app->user->identity->AlamatEmail ;?></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col s3 l3 center">
        <?= Html::img($gravatar, ['class'=>'responsive-img z-depth-1']) ;?>
    </div>
</div>
<div class="row">
    <div class="col s12 l12">
        <ul class="tabs">
            <li class="tab col s6"><a href="#jurnal" class="">Validasi Jurnal</a></li>
            <li class="tab col s6"><a href="#hibah" class="">Validasi Hibah</a></li>
        </ul>
    </div>
</div>
<div id="jurnal" class="row">
    <div class="col s12 black-text">
        <p align="justify">Syarat Layak Memenuhi Point Jurnal Pada Penilaian Objektif Sidang adalah sebagai berikut:</p>
        <div align="justify">
            <ul>
                <li>Paper telah lolos dan terbit pada Jurnal yang sudah terdaftar dalam <?= Html::a('SK Perguruan Tinggi Raharja 821 Tentang Ketentuan Jurnal Ilmiah Yang Diakui di Perguruan Tinggi Raharja.', Yii::$app->params['Url_SKJurnal'], ['target'=>'_blank'])?></li>
            </ul>
        </div>
        <hr>
        <p align="justify">Setelah dilakukan validasi, hasilnya adalah:</p>
        <p align="justify">Point Jurnal dalam Penilaian Objektif Sidang: <?= $POSidang_Jurnal?></p>
        <?php if(isset($jurnal)) {?>
        <p align="justify">Adapun detail dari keabsahan jurnal Anda adalah sebagai berikut:</p>
        <ul>
        <?php foreach($jurnal as $row) { ?>
            <li><span class="teal-text"><i class="material-icons">done</i><?=$row['JudulPaper']?> dari <?=$row['NamaJurnal']?> telah dinyatakan valid <?=date("d M Y",$row['TanggalAcc'])?> (<?= \davidhirtz\yii2\timeago\Timeago::tag($row['TanggalAcc']);?>).</span></li>
        <?php } } ?>
        </ul>
        <p align="justify">Berdasarkan hasil validasi, dinyatakan bahwa Pribadi Raharja di atas <?=$validasiJurnal?> point Jurnal pada Penilaian Objektif Sidang.</p>
    </div>
</div>
<div id="hibah" class="row">
    <div class="col s12 black-text">
        <p align="justify">Syarat Layak Memenuhi Point Hibah Pada Penilaian Objektif Sidang adalah sebagai berikut:</p>
        <div align="justify">
            <ul>
                <li>Hibah yang telah dinyatakan lolos dan sudah melalui verifikasi keabsahan PIC PESSTA+.</li>
            </ul>
        </div>
        <hr>
        <p align="justify">Setelah dilakukan validasi, hasilnya adalah:</p>
        <p align="justify">Point Hibah dalam Penilaian Objektif Sidang: <?= $POSidang_Hibah?></p>
        <?php if(isset($hibah)) {?>
        <p align="justify">Adapun detail dari keabsahan hibah kamu sebagai berikut:</p>
        <ul>
        <?php foreach($hibah as $row) { ?>
            <li><span class="teal-text"><i class="material-icons">done</i><?=$row['TempatInstansi']?> telah dinyatakan valid <?=date("d M Y",strtotime($row['TanggalAcc']))?> (<?=Yii::app()->format->timeago($row['TanggalAcc'])?>).</span></li>
        <?php } } ?>
        </ul>
        <p align="justify">Berdasarkan hasil validasi, dinyatakan bahwa mahasiswa tersebut <?=$validasiHibah?> point Jurnal pada Penilaian Objektif Sidang.</p>
    </div>
</div>