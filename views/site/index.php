<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
$this->title = 'Beranda';
$this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/easy-autocomplete.min.css');
$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/jquery.easy-autocomplete.min.js');
$this->registerAssetBundle(yii\web\JqueryAsset::className(), View::POS_HEAD);
?>
<div class="site-index">
    <div id="index-banner" class="">
        <div class="section no-pad-bot">
          <div class="container">
              <div class="header center"><img class="responsive-img" title=<?=Html::encode(\Yii::$app->name)?> src="https://lh3.googleusercontent.com/-Z25U3j2jILI/VvkHUS3NRtI/AAAAAAAAGFA/VT2rF_rqEKs5_Uh21SfLRKZQTGstJ88SwCCo/s512-Ic42/logopessta%252Breal.png"></img></div>
          </div>
        </div>
        <div class="row">
            <div class="col l6 push-l3">
                <?php $form = ActiveForm::begin([
                    'id' => 'search-form',
                    'options' => ['class' => 'form-horizontal'],
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-lg-12\">{input}</div>\n",
                        'labelOptions' => ['class' => 'col-lg-1 control-label'],
                    ],
                ]); ?>
                    <?= $form->field($model, 'NamaLengkap')->textInput(['autofocus' => true, 
                                                                        'id'=>'provider-remote',
                                                                        'required'=>'required',
                                                                        'aria-required'=>'true',
                                                                        'placeholder'=>'Ketikkan nama seseorang lalu tekan Enter',
                                                                        'autocomplete'=>'off'])->label(false); ?>
                <?php                     
                    $this->registerJs(
			'var options = {
                                url: function(phrase) {
                                        return "//rapi.raharja.me/JSON/qTWfK1EAHUWcLzSxnIWunTSlnzR/NamaLengkap/" + phrase;
                                },
                                getValue: "NamaLengkap",
                                requestDelay: 500,
                                template: {
                                        type: "description",
                                        fields: {
                                            description: "AlamatEmail"
                                        }
                                },
                                list: {
                                        maxNumberOfElements: 8,
                                        match: {
                                                enabled: true
                                        },
                                        sort: {
                                                enabled: false
                                        },
                                        showAnimation: {
                                                type: "fade",
                                                time: 400,
                                                callback: function() {}
                                        },
                                        hideAnimation: {
                                                type: "slide",
                                                time: 400,
                                                callback: function() {}
                                        }
                                }
                        };
			$("#provider-remote").easyAutocomplete(options);', View::POS_END, 'provider-remote');
                    $this->registerJs('window.onload = function() {
                            document.getElementById("provider-remote").focus();
                          };', View::POS_BEGIN);
                 ?>
                <?php ActiveForm::end(); ?>
            </div>            
        </div>
        <div class="row center">
            <a href="http://iran.ilearning.me/2016/04/15/video-tutorial-pessta/" id="download-button" class="btn-large waves-effect waves-light teal lighten-1" target="_blank">Get Started For PESSTA+</a>
        </div>
        <br><br>
    </div>
    <div class="body-content">
        <div class="row">
            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center teal-text"><i class="mdi-device-devices"></i></h2>
                    <h5 class="center">Konsep Desain</h5>
                    <p class="light center">Memanfaatkan konsep Desain Material Google, PESSTA+ coba menciptakan kerangka yang menggabungkan komponen dan animasi yang menyediakan lebih banyak interaksi kepada pengguna.</p><!--Selain itu, sistem responsif yang mendasarinya di seluruh platform memungkinkan untuk pengalaman pengguna yang lebih baik.</p-->
                </div>
            </div>
            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center teal-text"><i class="mdi-action-account-child"></i></h2>
                    <h5 class="center">Viewboard</h5>
                    <p class="light center">Tercatat hingga saat ini, <?=date('d M Y', time())?>, terdapat sebanyak <strong></strong> pengguna aktif dengan total submit jurnal ada <strong>x</strong>. Dengan <strong></strong> yang paling banyak disubmit mahasiswa. 
                        Detail selengkapnya <strong><a class="black-text" href="<?=Yii::$app->getUrlManager()->createUrl(['/viewboard/index'])?>">di sini</a></strong>.</p>
                </div>
            </div>
            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center teal-text"><i class="mdi-action-bug-report"></i></h2>
                    <h5 class="center">Kontak</h5>
                    <p class="light center">Tim PESSTA+ selalu berusaha untuk menyediakan dokumentasi rinci guna membantu pengguna baru. Kami, Tim PESSTA+, juga selalu terbuka untuk menerima <a class="black-text" href="<?=Yii::$app->getUrlManager()->createUrl(['/site/contact'])?>">kritik dan saran</a>.</p>
                </div>
            </div>
        </div>
    </div>
</div>
