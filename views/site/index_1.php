<?php
/* @var $this yii\web\View */

$this->title = 'Beranda';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">
        <div class="row">
            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center teal-text"><i class="mdi-device-devices"></i></h2>
                    <h5 class="center">Konsep Desain</h5>
                    <p class="light center">Memanfaatkan konsep Desain Material Google, PESSTA+ coba menciptakan kerangka yang menggabungkan komponen dan animasi yang menyediakan lebih banyak interaksi kepada pengguna.</p><!--Selain itu, sistem responsif yang mendasarinya di seluruh platform memungkinkan untuk pengalaman pengguna yang lebih baik.</p-->
                </div>
            </div>
            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center teal-text"><i class="mdi-action-account-child"></i></h2>
                    <h5 class="center">Viewboard</h5>
                    <p class="light center">Tercatat hingga saat ini, <?=date('d M Y', time())?>, terdapat sebanyak <strong></strong> pengguna aktif dengan total submit jurnal ada <strong>x</strong>. Dengan <strong></strong> yang paling banyak disubmit mahasiswa. 
                        Detail selengkapnya <strong><a class="black-text" href="<?=Yii::$app->getUrlManager()->createUrl(['/viewboard/index'])?>">di sini</a></strong>.</p>
                </div>
            </div>
            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center teal-text"><i class="mdi-action-bug-report"></i></h2>
                    <h5 class="center">Kontak</h5>
                    <p class="light center">Tim PESSTA+ selalu berusaha untuk menyediakan dokumentasi rinci guna membantu pengguna baru. Kami, Tim PESSTA+, juga selalu terbuka untuk menerima <a class="black-text" href="<?=Yii::$app->getUrlManager()->createUrl(['/site/contact'])?>">kritik dan saran</a>.</p>
                </div>
            </div>
        </div>
    </div>
</div>
