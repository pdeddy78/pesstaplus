<?php
$configs = parse_ini_file('/var/secure/hello.ini', true);
$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'name' => 'PESSTA+',
    'language' => 'id',
    'timezone' => 'Asia/Jakarta',
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            'site/*',
            'admin/*',
            'gii/*',
            'debug/*',
            'jurnal/*',
            'user/*',
            'api/*',
            'noty/*',
            'validasi/*',
            'mail/*',
            // The actions listed here will be allowed to everyone including guests.
            // So, 'admin/*' should not appear here in the production, of course.
            // But in the earlier stages of your development, you may probably want to
            // add a lot of actions here until you finally completed setting up rbac,
            // otherwise you may not even take a first step.
        ]
    ],
    'modules' => [
        'api' => [
            'class' => 'app\modules\api\Module',
            'controllerMap' => [
                'jurnalMaster' => 'app\modules\api\controllers\JurnalMasterController',
            ],
        ],
        'noty' => [
            'class' => 'lo\modules\noty\Module',
        ],
        'uploadDir' => '@webroot/uploads',
        'uploadUrl' => '/pesstaplus/uploads',
        'jurnal' => [
            'class' => 'app\modules\jurnal\Module',
            'controllerMap' => [
                'jurnalUpload' => 'app\modules\jurnal\controllers\JurnalUploadController',
                'jurnalMaster' => 'app\modules\jurnal\controllers\JurnalMasterController'
            ],
        ],
        'admin' => [
            'class' => 'mdm\admin\Module',
            'controllerMap' => [
                'assignment' => [
                    'class' => 'mdm\admin\controllers\AssignmentController',
                    /* 'userClassName' => 'app\models\User', */ // fully qualified class name of your User model
                    // Usually you don't need to specify it explicitly, since the module will detect it automatically
                    'idField' => 'ID',        // id field of your User model that corresponds to Yii::$app->user->id
                    'usernameField' => 'Username', // username field of your User model
                    'fullnameField' => 'profile.NamaLengkap',
                    'extraColumns' => [
                        [
                            'attribute' => 'NamaLengkap',
                            'label' => 'Nama Lengkap',
                            'value' => function($model, $key, $index, $column) {
                                return $model->profile->NamaLengkap;
                            },
                        ],
                    ],
                    'searchClass' => 'app\models\UserSearch'    // fully qualified class name of your User model for searching
                ]
            ],
            'layout' => 'top-menu',
            'menus' => [
                'assignment' => [
                    'label' => 'Grant Access' // change label
                ],
                'route' => null, // disable menu
                'rule' => null,  // disable menu
                'menu' => null  // disable menu
            ],
        ]
    ],
    'components' => [
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => null,   // do not publish the bundle
                    'js' => [
                        'https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js',
                    ]
                ],
                'davidhirtz\yii2\timeago\TimeagoAsset'=>[
                    // Load localized version based on Yii::$app->language. Default true.
                    'locale'=>true,
                    // Use short locale version if available. Default false.
                    'short'=>false,
                    // Plugin options, see plugin website for details. Default values below.
                    'settings'=>[
                        'refreshMillis'=>60000,
                        'allowPast'=>true,
                        'allowFuture'=>false,
                        'localeTitle'=>false,
                        'cutoff'=>0,
                        'autoDispose'=>true,
                        // Strings set here it will overwrite loaded locale config.
                        'strings'=>[
                            'prefixAgo'=>null,
                            'prefixFromNow'=>null,
                            'suffixAgo'=>"ago",
                            'suffixFromNow'=>"from now",
                            'inPast'=>'any moment now',
                            'seconds'=>"less than a minute",
                            'minute'=>"about a minute",
                            'minutes'=>"%d minutes",
                            'hour'=>"about an hour",
                            'hours'=>"about %d hours",
                            'day'=>"a day",
                            'days'=>"%d days",
                            'month'=>"about a month",
                            'months'=>"%d months",
                            'year'=>"about a year",
                            'years'=>"%d years",
                            'wordSeparator'=>" ",
                            'numbers'=>[],
                        ],
                    ],
                ],
            ],
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
            'cookieValidationKey' => 'cinLRoEvrn5UUNYtpEneLlJiNTj22cCc',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/fault',
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
		'google' => [
                    'class'  => 'yii\authclient\clients\Google',
                    'clientId'  => $configs['oauth_google_clientId'],
                    'clientSecret' => $configs['oauth_google_clientSecret'],
		]
            ],
        ],
	'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/mail',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => $configs['smtp_host'],
                'username' => $configs['smtp_username'],
                'password' => $configs['smtp_password'],
                'port' => '2525',//'587'
                'encryption' => 'tls',
            ],
        ],
        'formatter' => [
            'dateFormat' => 'php:d-m-Y',
            'datetimeFormat'=>'php:d/m/y H:i:s'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
               //   'class' => 'yii\log\DbTarget',
                    'prefix' => function ($message) {
                        $user = Yii::$app->has('user', true) ? Yii::$app->get('user') : null;
                        $userID = $user ? $user->getId(false) : '-';
                        return "[$userID]";
                    },
                    'levels' => ['error', 'warning'],
                    'categories' => [
                        'yii\db\*',
                        'yii\web\HttpException:*',
                        'yii\swiftmailer\Logger::add'
                    ],
                    'except' => [
                        'yii\web\HttpException:404',
                    ],
                ],
                [
                    'class' => 'yii\log\EmailTarget',
                    'levels' => ['error'],
                    'categories' => ['yii\db\*'],
                    'message' => [
                       'from' => [$configs['email_official']],
                       'to' => [$configs['email_admin']],
                       'subject' => 'Database errors at PESSTA+',
                    ],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
                            
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
        ],
        
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                //'<controller:\w+>/<id:\d+>'                 => '<controller>/view',
                'user/<username:\w+>' => 'user/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>'  => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>'    => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>'             => '<controller>/<action>',
                '<module>/<controller>/<action>/<id:\d+>' => '<module>/<controller>/<action>',
                //'<module:\w+>/<action:\w+>' => '<module>/default/<action>',
                '<module:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
                'GET <module:api>/<controller:[\w-]+>' => '<module>/<controller>/index',
                'POST <module:api>/<controller:[\w-]+>' => '<module>/<controller>/create',
                'GET <module:api>/<controller:[\w-]+>/<id:\d+>' => '<module>/<controller>/view',
                'PUT <module:api>/<controller:[\w-]+>/<id:\d+>' => '<module>/<controller>/update',
                'DELETE <module:api>/<controller:[\w-]+>/<id:\d+>' => '<module>/<controller>/delete',
                'POST <module:api>/<controller:[\w-]+>/<action:validate>' => '<module>/<controller>/<action>',
                '<module:api>/<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:api>/<controller:[\w-]+>/<action:[\w-]+>' => '<module>/<controller>/<action>',
                [
                    'class' => 'yii\rest\UrlRule',
                    'pluralize' => false,                   
                    'controller' => 'jurnal/masterRest'
                ],
            ],
        ],         
        'view' => [
            'class' => 'yii\web\View',
            'theme' => [
                'basePath' => '@app/themes/materialize',
                'baseUrl' => '@web/themes/materialize',
                'pathMap' => [
                    '@app/views' => '@app/themes/materialize'
                ],
            ]
        ],
    ],
    'params' => $params,
    'controllerMap' => [
        'LogAction' => [ // Fixture generation command line.
            'class' => 'app\controllers\LogActionController',
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['127.0.0.1', '::1', $_SERVER['REMOTE_ADDR']],
        'generators' => [
            'crud' => [
                'class' => 'yii\gii\generators\crud\Generator',
            ]
        ],
    ];
}

return $config;