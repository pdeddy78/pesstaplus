<?php
$configs = parse_ini_file('/var/secure/hello.ini', true);
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2_pesstaplus',
    'username' => $configs['db_uname'],
    'password' => $configs['db_pwd'],
    'charset' => 'utf8',
    'enableSchemaCache' => true,
    // Duration of schema cache.
    'schemaCacheDuration' => 3600,
    // Name of the cache component used to store schema information
    'schemaCache' => 'cache',
];
