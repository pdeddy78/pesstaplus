<?php

namespace app\controllers;
use Yii;
use app\modules\jurnal\models\JurnalUpload;

class ValidasiController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $gravatar = md5(trim(Yii::$app->user->identity->AlamatEmail));
        $gravatar = 'http://www.gravatar.com/avatar/'.$gravatar.'?s=400.png';
        $email = Yii::$app->user->identity->AlamatEmail;
        $cekJurnal = $this->cekJurnal($email);
        if($cekJurnal != FALSE)
        {
            $validasiJurnal = "<span class='teal-text'><b>LAYAK</b></span>/<s>TIDAK LAYAK</s>";
            $POSidang_Jurnal = "<b>".Yii::$app->params['POSidang_Jurnal']."</b>";
        }
        else
        {
            $validasiJurnal = "<s>LAYAK</s>/<span class='red-text red-darken-3'><b>TIDAK LAYAK</b></span>";
            $POSidang_Jurnal = "<b>0</b>";
            Yii::$app->session->setFlash('warning','Segera submit jurnal <br/> untuk penuhi poinmu!');
        }
        
        return $this->render('index', [ 'validasiJurnal' => $validasiJurnal,
                                        'POSidang_Jurnal' => $POSidang_Jurnal,
                                        'jurnal' => $cekJurnal,
                                        'gravatar' => $gravatar
                                      ]);
    }

    public static function cekJurnal($email)
    {
        $cek = JurnalUpload::find()
                    ->where('AlamatEmail = :AlamatEmail', [':AlamatEmail' => $email])
                    ->andWhere('StatusPaper = :StatusPaper', [':StatusPaper' => '1'])
                    ->one();
        if($cek)
        {
            return JurnalUpload::find()
                    ->select('JudulPaper, IDJurnal, TanggalAcc')
                    ->where('AlamatEmail = :AlamatEmail', [':AlamatEmail' => $email])
                    ->andWhere('StatusPaper = :StatusPaper', [':StatusPaper' => '1'])
                    ->asArray()
                    ->all();
        }
        else return FALSE;
    }
    
    public static function cekHibah($email)
    {
        return HibahUpload::find()
                    ->where('AlamatEmail = :AlamatEmail', [':AlamatEmail' => $email])
                    ->andWhere('StatusPaper = :StatusPaper', [':StatusPaper' => '1'])
                    ->asArray()
                    ->all();
    }
}