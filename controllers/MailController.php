<?php

namespace app\controllers;
use Yii;
/**
 * Default controller for the `jurnal` module
 */
class MailController extends \app\components\BaseController
{

    public static function sendMail($email, $subject, $from, $view, $model)
    {        
        $emailSend = Yii::$app->mailer->compose($view, ['model' => $model /*'model'=> $models*/])
        ->setFrom([\Yii::$app->params['adminEmail'] => $from])
        ->setTo($email)
        ->setHeader('PESSTA+', 'Pemberitahuan Surel PESSTA+')
        ->setSubject($subject);
                
        return $emailSend->send();
    }

}
