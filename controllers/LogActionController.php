<?php

namespace app\controllers;

use Yii;
use app\models\LogAction;
use app\models\LogActionSearch;
use yii\web\NotFoundHttpException;

/**
 * LogActionController implements the CRUD actions for LogAction model.
 */
class LogActionController extends \app\components\BaseController
{
    /**
     * @inheritdoc
     */


    /**
     * Lists all LogAction models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LogActionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LogAction model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    /**
     * Finds the LogAction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LogAction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LogAction::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
