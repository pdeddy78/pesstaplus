<?php

namespace app\controllers;

use Yii;
//use yii\filters\AccessControl;
//use yii\filters\VerbFilter;
//use yii\web\Controller;
//use app\models\LoginForm;
use app\models\ContactForm;
use app\models\ValidasiForm;
use app\models\Profile;
use app\models\User;
use yii\helpers\Url;


class SiteController extends \app\components\BaseController
{    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            /*
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],*/
        ];
    }
    
    public function actionFault()
    {
        $exception = Yii::$app->errorHandler->exception;

        if ($exception !== null) {
            $statusCode = $exception->statusCode;
            $name = $exception->getName();
            $message = $exception->getMessage();
            $this->logMessage=$message;

            return $this->render('error', [
                'exception' => $exception,
                'statusCode' => $statusCode,
                'name' => $name,
                'message' => $message
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'successCallback'],
            ],
            /*
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ], */
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    public function safeAttributes($client)
    {
        // get user data from client
        $attributes = $client->getUserAttributes();
        // set default value
        $safe_attributes = [
            'provider'=> '',
            'id'=> '',
            'username'=> '',
            'name'=> '',
            'email'=> '',
        ];

        // get value from user attributes base on social media
        if ($client instanceof \yii\authclient\clients\Google) {
            $safe_attributes = [
                'provider'=> 'google',
                'id'=> $attributes['id'],                
                'email'=> $attributes['emails']['0']['value'],
                'name'=> $attributes['displayName'],
                
            ];
            $email_explode = explode('@', $safe_attributes['email']);
            $safe_attributes['username'] = $email_explode[0];       
        } 

        return $safe_attributes;
    }
    
    public function successCallback($client) 
    {
        // call safeAttributes method for properly format data
        $attributes = $this->safeAttributes($client);
        
        $email_explode = explode('@', $attributes['email']);
        if($email_explode[1] === 'raharja.info') 
        {        
            // check if email exists in tabel user
            $user = User::find()
                        ->where([
                          'AlamatEmail' => $attributes['email']
                        ])->one();
            if($user)
            {   
                //$NIPR = User::findNIPR($attributes['email']);                
                Yii::$app->user->login($user);
            }
            else
            {
                // check if social media not twiiter
                if($attributes['provider']!='twitter')
                {
                    // do automatic signup
                    $user = new User([
                        'Username' => $attributes['username'],
                        'AlamatEmail' => $attributes['email'],
                        'Bergabung' => time(),
                        'Kunjungan' => time(),

                    ]);
                    $user->generateAuthKey();
                    if($user->save())
                    {
                        $profile = new Profile([
                            'IDRinfo'=>(string)$attributes['id'],
                            'AlamatEmail' => $attributes['email'],
                            'NamaLengkap' => $attributes['name'],
                            'KodeUser'=>$user->id,
                        ]);
                        $profile->save();
                        Yii::$app->user->login($user);
                    }
                    else
                    {
                        Yii::$app->session->setFlash('error','Login gagal, error saat registrasi');
                    }
                }
                else{
                    // save data attributes to session
                    $session = Yii::$app->session;
                    $session['attributes']=$attributes;

                    // redirect to signup, via property successUrl
                    $this->action->successUrl = Url::to(['signup']);
                }
            }
            // update login lastvisit
            $user->Kunjungan = time();
            $user->save();
        }
        else
        {
            Yii::$app->session->setFlash('error','Login gagal, hanya untuk Pribadi Raharja saja.');
            $this->actionLogout();
        }
    }
    
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if(!Yii::$app->user->isGuest)
        {
            $this->redirect(['/validasi']);
        }
        else
        {
            // start
            // important search-module
            // if you made change of this, you must copy & paste to actionIndex/Home function
            $session = Yii::$app->session;
            $session->open();
            $session['search'] = [
                    'NIPR' => null,
                    'NamaLengkap' => null,
                    'AlamatEmail' => null
                ];
            
            $model = new ValidasiForm();
            
            if ($model->load(Yii::$app->request->post()) && $model->validate()) 
            {
                $json = $model->getDtPribadiRaharja();
                $NamaLengkap = urldecode(rtrim($model->NamaLengkap));
                if($json != FALSE)
                {
                    //die(\yii\helpers\VarDumper::dump($NamaLengkap));
                    foreach ($json as $key => $value) {
                        if(rtrim($value['NamaLengkap']) == $NamaLengkap && strpos($value['AlamatEmail'],'@raharja.info') !== false)
                        {
                            $session['search'] = [
                                'NIPR' => rtrim($value['NIPR']),
                                'NamaLengkap' => rtrim($value['NamaLengkap']),
                                'AlamatEmail' => strtolower(rtrim($value['AlamatEmail']))
                            ];
                        }
                        elseif(stripos(strtolower($value['NamaLengkap']), strtolower($NamaLengkap)) !== false && strpos($value['AlamatEmail'],'@raharja.info') !== false)
                        {
                            $session['search'] = [
                                'NIPR' => rtrim($value['NIPR']),
                                'NamaLengkap' => rtrim($value['NamaLengkap']),
                                'AlamatEmail' => strtolower(rtrim($value['AlamatEmail']))
                            ];
                        }
                        elseif($value['AlamatEmail'] == null)
                        {
                            $session['search'] = [
                                'AlamatEmail' => null
                            ];							
                        }
                    }
                    //hentikan bila tak ditemukan Rinfo
                    if($session['search']['AlamatEmail'] == null)
                    {
                        Yii::$app->session->setFlash('error', 'Maaf, tidak dapat menerima service dengan data tersebut. Pastikan yang bersangkutan telah mendapatkan username dengan request email Rinfo.');
                        return $this->render('index', ['model'=>$model]);
                    }
                    else
                    {
                        $gravatar = md5(trim($session['search']['AlamatEmail']));
                        $gravatar = 'http://www.gravatar.com/avatar/'.$gravatar.'?s=400.png';
                        //die(\yii\helpers\VarDumper::dump($session['search']));
                        $cekJurnal = ValidasiController::cekJurnal($session['search']['AlamatEmail']);
                        if($cekJurnal != FALSE)
                        {
                            $validasiJurnal = "<span class='teal-text'><b>LAYAK</b></span>/<s>TIDAK LAYAK</s>";
                            $POSidang_Jurnal = "<b>".Yii::$app->params['POSidang_Jurnal']."</b>";
                        }
                        else
                        {
                            $validasiJurnal = "<s>LAYAK</s>/<span class='red-text red-darken-3'><b>TIDAK LAYAK</b></span>";
                            $POSidang_Jurnal = "<b>0</b>";
                            Yii::$app->session->setFlash('warning','Segera submit jurnal <br/> untuk penuhi poinmu!');
                        }
                    }
                    return $this->render('@app/views/validasi/PribadiRaharja', [
                                            'model'=>$model,
                                            'gravatar' => $gravatar,
                                            'validasiJurnal' => $validasiJurnal,
                                            'POSidang_Jurnal' => $POSidang_Jurnal,
                                            'jurnal' => $cekJurnal
                                        ]);
                }                      
            }
            // end of search-function
            
            return $this->render('index', ['model'=>$model]);
        }
    }
    
    public function actionHome()
    {
        // start
        // important search-module
        // if you made change of this, you must copy & paste to actionIndex/Home function
        $session = Yii::$app->session;
        $session->open();
        $session['search'] = [
                'NIPR' => null,
                'NamaLengkap' => null,
                'AlamatEmail' => null
            ];
        
        $model = new ValidasiForm();        
        if ($model->load(Yii::$app->request->post()) && $model->validate()) 
        {
            $json = $model->getDtPribadiRaharja();
            $NamaLengkap = urldecode(rtrim($model->NamaLengkap));
            if($json != FALSE)
            {
                //die(\yii\helpers\VarDumper::dump($NamaLengkap));
                foreach ($json as $key => $value) {
                    if(rtrim($value['NamaLengkap']) == $NamaLengkap && strpos($value['AlamatEmail'],'@raharja.info') !== false)
                    {
                        $session['search'] = [
                            'NIPR' => rtrim($value['NIPR']),
                            'NamaLengkap' => rtrim($value['NamaLengkap']),
                            'AlamatEmail' => strtolower(rtrim($value['AlamatEmail']))
                        ];
                    }
                    elseif(stripos(strtolower($value['NamaLengkap']), strtolower($NamaLengkap)) !== false && strpos($value['AlamatEmail'],'@raharja.info') !== false)
                    {
                        $session['search'] = [
                            'NIPR' => rtrim($value['NIPR']),
                            'NamaLengkap' => rtrim($value['NamaLengkap']),
                            'AlamatEmail' => strtolower(rtrim($value['AlamatEmail']))
                        ];
                    }
                    elseif($value['AlamatEmail'] == null)
                    {
                        $session['search'] = [
                            'AlamatEmail' => null
                        ];							
                    }
                }
                //hentikan bila tak ditemukan Rinfo
                if($session['search']['AlamatEmail'] == null)
                {
                    Yii::$app->session->setFlash('error', 'Maaf, tidak dapat menerima service dengan data tersebut. Pastikan yang bersangkutan telah mendapatkan username dengan request email Rinfo.');
                    return $this->render('index', ['model'=>$model]);
                }
                else
                {
                    $gravatar = md5(trim($session['search']['AlamatEmail']));
                    $gravatar = 'http://www.gravatar.com/avatar/'.$gravatar.'?s=400.png';
                    //die(\yii\helpers\VarDumper::dump($session['search']));
                    $cekJurnal = ValidasiController::cekJurnal($session['search']['AlamatEmail']);
                    if($cekJurnal != FALSE)
                    {
                        $validasiJurnal = "<span class='teal-text'><b>LAYAK</b></span>/<s>TIDAK LAYAK</s>";
                        $POSidang_Jurnal = "<b>".Yii::$app->params['POSidang_Jurnal']."</b>";
                    }
                    else
                    {
                        $validasiJurnal = "<s>LAYAK</s>/<span class='red-text red-darken-3'><b>TIDAK LAYAK</b></span>";
                        $POSidang_Jurnal = "<b>0</b>";
                        Yii::$app->session->setFlash('warning','Segera submit jurnal <br/> untuk penuhi poinmu!');
                    }
                }
                return $this->render('@app/views/validasi/PribadiRaharja', [
                                        'model'=>$model,
                                        'gravatar' => $gravatar,
                                        'validasiJurnal' => $validasiJurnal,
                                        'POSidang_Jurnal' => $POSidang_Jurnal,
                                        'jurnal' => $cekJurnal
                                    ]);
            }                    
        }
        // end of search-function
        
        return $this->render('index', ['model'=>$model]);
    }

    /**
     * Login action.
     *
     * @return string
    
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }
     * */

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
