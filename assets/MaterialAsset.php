<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MaterialAsset extends AssetBundle
{
    public $basePath = '@app';// @webroot';
    public $baseUrl = '@web'; //@web';
    public $css = [
        'themes/materialize/css/style.css',
        'themes/materialize/css/materialize.min.css',
        'https://fonts.googleapis.com/icon?family=Material+Icons'
    ];
    public $js = [
        'themes/materialize/js/init.js',
        'themes/materialize/js/materialize.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
