<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tmprofile".
 *
 * @property integer $ID
 * @property string $IDRinfo
 * @property string $NamaLengkap
 * @property string $AlamatEmail
 *
 * @property Tmuser $kodeUser
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%TMProfile}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'IDRinfo', 'NamaLengkap', 'AlamatEmail'], 'required'],
            [['ID'], 'integer'],
            [['NamaLengkap', 'AlamatEmail'], 'trim'],
            [['IDRinfo', 'NamaLengkap', 'AlamatEmail'], 'string', 'max' => 255],
            [['ID'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['ID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'IDRinfo' => 'ID Rinfo',
            'NamaLengkap' => 'Nama Lengkap',
            'AlamatEmail' => 'Alamat Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['ID' => 'ID']);
    }
}
