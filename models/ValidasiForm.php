<?php

namespace app\models;

use Yii;
use yii\base\Model;

class ValidasiForm extends Model
{
    public $NamaLengkap;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['NamaLengkap'], 'required'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'NamaLengkap' => 'Telusuri Pribadi Raharja',
        ];
    }
    
    public function getDtPribadiRaharja()
    {
        $APIPribadiRaharja = Yii::$app->params['PribadiRaharjaByName'].rawurlencode($this->NamaLengkap);
        $getPribadiRaharja = file_get_contents($APIPribadiRaharja);
        $json = json_decode($getPribadiRaharja, true);
        
        uasort($json, function($item1, $item2){
            return $item1->ID < $item2->ID;
        });
        
        if(!empty($json))
        {
            return $json;
        }
        else return FALSE;
    }
}
