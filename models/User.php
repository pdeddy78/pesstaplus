<?php
namespace app\models;

use Yii;
use yii\web\IdentityInterface;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "tmuser".
 *
 * @property integer $KodeUser
 * @property string $Username
 * @property string $AlamatEmail
 * @property string $KunciOtentik
 * @property integer $Bergabung
 * @property integer $Kunjungan
 *
 * @property SocialAccount[] $socialAccounts
 * @property Tmprofile $tmprofile
 * @property Token[] $tokens
 */
class User extends ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%TMUser}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Username', 'AlamatEmail', 'KunciOtentik', 'Bergabung', 'Kunjungan'], 'required'],
            [['Bergabung', 'Kunjungan'], 'integer'],
            [['AlamatEmail'], 'trim'],
            [['Username', 'AlamatEmail'], 'string', 'max' => 255],
            [['KunciOtentik'], 'string', 'max' => 32],
            [['AlamatEmail'], 'unique'],
            [['Username'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Username' => 'Username',
            'AlamatEmail' => 'Alamat Email',
            'KunciOtentik' => 'Kunci Otentik',
            'Bergabung' => 'Bergabung',
            'Kunjungan' => 'Kunjungan',
        ];
    }
    public static function findIdentityByAccessToken($token, $type = null)
    {
    }
    
    public static function findIdentity($id)
    {
        return static::findOne(['ID' => $id]);
    }
    
    public static function findJenisKelamin($AlamatEmail)
    {
        $API_PribadiRaharja = file_get_contents(Yii::$app->params['PribadiRaharja'].$AlamatEmail);
	$PribadiRaharja = json_decode($API_PribadiRaharja, true);
        return $PribadiRaharja[0]['JenisKelamin'];
    }
    
    public static function findByUsername($username)
    {
        return static::findOne(['Username' => $username]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['ID' => 'ID']);
    }
    
    public function getNamaLengkap()
    {
        $profile = Profile::find()->where(['ID'=>$this->ID])->one();
        if ($profile !==null)
            return $profile->NamaLengkap;
        return false;
    }
        
    public function getNIPR()
    {
        $AlamatEmail = $this->AlamatEmail;
        $API_PribadiRaharja = file_get_contents(Yii::$app->params['PribadiRaharja'].$AlamatEmail);
	$PribadiRaharja = json_decode($API_PribadiRaharja, true);
        foreach($PribadiRaharja as $row){
            if(strtolower($row['AlamatEmail']) === $AlamatEmail)
            {
                $NIPR = $row['NIPR'];
            }
            else $NIPR = NULL;
        }
        
        return $NIPR;
    }
    
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->KunciOtentik;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function generateAuthKey()
    {
        $this->KunciOtentik = Yii::$app->security->generateRandomString();
    }

}
