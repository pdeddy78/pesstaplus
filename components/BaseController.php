<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\components;
use yii\web\Controller;
use Yii;

class BaseController extends Controller
{
    
    public $logMessage = NULL;
    public $writeLog = true;
    
    public function init()
    {
        parent::init();
    }    
    
    public function afterAction($action, $result) 
    {        
        if($this->writeLog)
        {
            $sql = "INSERT INTO {{TTLogAction}} (username, ipaddress, logtime, controller, action, details)
                    VALUES (:username, :ipaddress, :logtime, :controller, :action, :details)";
            $command = Yii::$app->db->createCommand($sql);
            $command->bindValues([
                ':username' => Yii::$app->user->isGuest ? 'Guest' : Yii::$app->user->identity->Username,
                ':ipaddress' => $_SERVER['REMOTE_ADDR'],
                ':logtime' => time(),
                ':controller' => Yii::$app->controller->id,
                ':action' => Yii::$app->controller->action->id,
                ':details' => $this->logMessage,
            ])->execute();            
        }
        return parent::afterAction($action, $result);
    }
}